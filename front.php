<?php
/*
 * Design Patter of Front Controller
 * http://en.wikipedia.org/wiki/Front_Controller_pattern
 */
ini_set( 'default_charset', 'utf-8');
setlocale(LC_ALL,'pt_BR.UTF8');
mb_internal_encoding('UTF8'); 
mb_regex_encoding('UTF8');

$PROJECT_NAME = 'dlite';
session_start();
$SYSTEM = '01';
//$json = json_decode(file_get_contents('http://dsandrde.info/api/controle?id='.$SYSTEM));

//Utilizado para a configuracao de moeda
setlocale(LC_MONETARY, 'pt_BR');

//Variaveis que teram o c�digo da p�gina
$GLOBALS['response'] = '';
$GLOBALS['js_call'] = '';
$GLOBALS['head'] = '';

include_once 'application_configuration.php';
require_once('lib/third/__init__.php');
include_once 'lib/base/__ini__.php';
require_path('lib/base');
require_path('lib/components');

$_GET['__key__'] = ($_GET['__key__'] != "") ? $_GET['__key__'] : 'login';
$uri = explode('/',$_GET['__key__']);
$controller_class = null;
$controller = null;
$num = count($uri);
$path = ''; 
$method;

if (is_post()) {
  $method = $_POST;
} else if (is_get()) {
  $method = $_GET;
} else if (is_put()) {
  $method = $_PUT;
} else if (is_delete()) {
  $method = $_DELETE;
}

$req = new Request($method);

if ($num == 1) {
	$path = $uri[0].'/';
	$controller_class = $map['/'.$uri[0]];

	if ($controller_class != ''){
		$req->setUrl('/'.$uri[0]);
		require_once('lib/controller/'.$path.$controller_class.'.php');
		$controller = new $controller_class($req);
	}
	
} else {
  	for ($i=0; $i<= $num-1; $i++) {
	  	if ($uri[$i]=='') {
	  	} else {
		  	$path .= '/'.$uri[$i];
	  	}
  	}
  	$controller_class = $map[$path];

  	if ($controller_class != '') {
			$req->setUrl($path);
  		require_once('lib/controller'.$path.'/'.$controller_class.'.php');
  		$controller = new $controller_class($req);
  	}
}

if (!isset($controller)) {
  send_redirect('/error');
} else {
  if ($controller instanceof Controller) {
    $controller->processRequest();
  }

	if ($controller instanceof WebController) {
    $GLOBALS['response'] .= $controller->composePage();
  } 

  if ($controller instanceof RestController) {
   echo json_encode($controller->restResponse());
	}

	if ($controller instanceof WebController) {
		js_call("$('#msg_system').hide();");
		js_call("$('#msg_system').fadeIn(1000);");
		js_call("$('#msg_system').fadeOut(10000);");
	}
  echo $GLOBALS['response'].$GLOBALS['js_call'];
}
