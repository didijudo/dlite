function exclusao(id) {
	$.ajax({
		type: 'POST',
		url: '/excluir/custo', 
	 	data: {'id': id},
		dataType: 'json',
	 	success: function(data) {
			console.log(data);
			if (data[0].result === 'ok') {
				exibaMensagem('Exclusão', 'Custo excluido com sucesso.', false, true);	
			}
			if (data[0].result === 'error') {
				exibaMensagem('ERRO', 'Erro ao excluir o custo. '+data[0].msg);	
			}
		}
	});
}

function excluir(id) {
	exibaConfirmacao(
		'Excluir',
		'Deseja realmente excluir este custo?',
		false,
		false,
		exclusao,
		id);
}
