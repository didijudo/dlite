function exclusao(id) {
	$.ajax({
		type: 'POST',
		url: '/excluir/programa',
	 	data: {'id': id},
		dataType: 'json',
	 	success: function(data) {
			console.log(data);
			if (data[0].result === 'ok') {
				exibaMensagem('Excluir', 'Programa excluido com sucesso.');	
			}
			if (data[0].result === 'error') {
				exibaMensagem('ERRO', 'Erro ao excluir o programa');
			}
		}
	});
}

function excluir(id) {
	exibaConfirmacao(
		'Excluir', 
		'Deseja realmente excluir esse programa?',
		false,
		true,
		exclusao,
		id);
}
	
