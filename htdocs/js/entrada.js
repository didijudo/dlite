$(document).ready(function() {
	$('#idPrograma').change(function() {
		$.getJSON('/api/conta', {'programa': this.value}, function(data) {
			$('#idConta').empty();
				$('#idConta').append(
					"<option> Selecione uma conta </option>"
				);
			for(var i=0; i < data.length; i++) {
				$('#idConta').append(
					"<option value="+data[i].id+"> "+	
						data[i].nome+" | Ag: "+data[i].agencia+ " | numero: "+ data[i].numero +
						"</option>"
				);
			}
		});
	});
});
