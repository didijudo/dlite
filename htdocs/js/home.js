var datax = [];
$(document).ready(function() {
	function montaGrafico(datax) {
		$.plot($("#grafico"), datax, {
			series: {
				pie: {
					innerRadius: 0.4,
					show: true
				}
			},
			legend: {
				show: true
			},
			grid: {
				hoverable: true
			},
			tooltip: true,
			tooltipOpts: {
				content: function(label, xval, yval) {
					var content = yval +"% %s"; 
					return content;
				}
			}
		});
	}
	$.getJSON('api/valores', {}, function(data) {
		for (var i=0; i < data.length; i++ ) {
			datax[i] = {
				label: data[i].nome, data: data[i].percent.toFixed(2), color: data[i].cor 
			};
		}
		montaGrafico(datax);
	});
});
