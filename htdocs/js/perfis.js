function exclusao(id) {
	$.ajax({
		type: 'POST',
		url: '/excluir/perfil', 
	 	data: {'id': id},
		dataType: 'json',
	 	success: function(data) {
			if (data[0].result === 'ok') {
				exibaMensagem('Exclusão', 'Perfil excluido com sucesso.', false, true);	
			}
			if (data[0].result === 'error') {
				exibaMensagem('ERRO', 'Erro ao excluir o perfil. '+data[0].msg);	
			}
		}
	});
}

function excluir(id) {
	exibaConfirmacao(
		'Excluir',
		'Deseja realmente excluir este perfil?',
		false,
		false,
		exclusao,
		id);
}
