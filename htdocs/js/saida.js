$(document).ready(function() {
	$("#idCusto").autocomplete({
		minLength: 3,
		source: function(request, response) {
			$.ajax({async: true, cache: false, type: "get", dataType: "json",
				url: "/api/autocomplete/custo",
				data: {"nome": $('#idCusto').val(), "tipo": 2},
				success: function(data) {
					response(data);
				}
			});
		},
		select: function(event, ui) {
			$("#idCustoValue").val(ui.item.id);
		}
	});

	$("#idFornecedor").autocomplete({
		minLength: 3,
		source: function(request, response) {
			$.ajax({async: true, cache: false, type: "get", dataType: "json",
				url: "/api/autocomplete/fornecedor",
				data: {"nome": $('#idFornecedor').val()},
				success: function(data) {
					response(data);
				}
			});
		},
		select: function(event, ui) {
			$("#idFornecedorValue").val(ui.item.id);
		}
	});

	dialog = $('#dialog').dialog({
		autoOpen: false,
		height: 250,
		width: 350,
		title: 'Novo Fornecedor',
		modal: true,
		create: function() {
			$(".ui-dialog").find(".ui-dialog-titlebar-close").css({
				'background-image': 'none',
				'background-color': 'white',
				'border': 'none'
			});
		},
		buttons: {
			"Cadastrar": function() {
				var nome = $('#idNomeFor').val();
				var cnpj = $('#idCnpj').val();
				console.log(nome + "   -   "+ cnpj);
				if (nome === '') {
					alert('Digite um nome para o fornecedor');
					return;
				}
				if (cnpj === '') {
					alert('Digite um cnpj para o fornecedor');
					return;
				}
				$.post(
					'/cadastrar/fornecedor', 
					{
						'nome': nome, 
						'cnpj': $('#idCnpj').val()
					},
					function(data, status) {
						if (status === 'success') {
							alert('Fornecedor cadastrado com sucesso');
							dialog.dialog('close');
						} else {
							alert('Não foi possível cadastrar o fornecedor no momento');
							dialog.dialog('close');
						}
					});
			},
			"Cancelar": function() {
				dialog.dialog( "close" );
			},
		},
		close: function() {
			document.forms[1].reset();
		}
	});	

	$('#idNovoFornecedor').button().on( "click", function() {
		dialog.dialog( "open" );
	});
});
