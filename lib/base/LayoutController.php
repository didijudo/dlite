<?php
class LayoutController extends WebController {
    
  protected function setTitle() {
    return '::Finanças::';
  }

  protected function composeBody() {
    $html = 
      <x:frag>
				<body class="infobar-offcanvas">
					<d:fixed/>
					{$this->composeMenuTop()}
					<div id="wrapper">
						<div id="layout-static">
								{$this->composeMenu()}
							<div class="static-content-wrapper">
								<div class="static-content">
									<div class="page-content">
										{$this->getMsg()}
										{$this->setContent()}
									</div>
								</div>
								{$this->composeFooter()}
							</div>
						</div>
					</div>
				</body>
			</x:frag>;
    return $html;
  }

  protected function setContent() {
    return
      <x:frag>
				<div class="page-heading">            
					<h1>Dashboard</h1>
				</div>
				<div class="container-fluid">
					<div data-widget-group="group1">
					</div>
				</div> 
      </x:frag>;
  }

  protected function composeMenuTop() {
    return
      <x:frag>
				<d:menu-top/>
      </x:frag>;
  }

  protected function composeMenu() {
    return
      <x:frag>
				<d:menu-left/>
      </x:frag>;
  }

  protected function composeFooter() {
    return
      <x:frag>
				<footer role="contentinfo">
					<div class="clearfix">
						<ul class="list-unstyled list-inline pull-left">
							<li><h6 style="margin: 0;"> &copy; 2015 - {date('Y')} Somar Gestão pública</h6></li>
						</ul>
						<button 
							class="pull-right btn btn-link btn-xs hidden-print" 
							id="back-to-top">
								<i class="fa fa-arrow-up"></i>
						</button>
					</div>
				</footer>
      </x:frag>;
  }
}
