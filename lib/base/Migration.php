<?php
abstract class Migration {

  abstract protected function run();

  public function go($conn) {
    try {
      $result = $conn->executeQuery($this->run());
    } catch(Exception $e) {
      return false;
    }
    return $result;
  }

	protected function undo() {}
}
