<?php
class WebController extends Controller {
  public $js;
  public $css;

  public final function composePage() {
    $html = '<!DOCTYPE html>';
    $html .= '<html lang="pt-br">';
    $html .= '<meta http-equiv="Content-Type" 
      content="text/html;charset=utf8">';
    $html .= '<head>';
    //$html .= '<link rel="icon" href="'.get_image("favicon.ico").'" type="image/x-icon"/>';
    $html .= '<title>'.$this->setTitle().'</title>';
    $html .= '<meta name="viewport" content="width=device-width">';
    $html .= $this->setHead();
    $html .= $this->setCss();
    $html .= $this->setJS();
    $html .= '</head>';
    $html .= (string)$this->composeBody();
    $html .= '</html>';
    return $html;
  }

/*
 * This function can be fragmented in minimal functions to make a page
 */

  protected function composeBody() {
  }

  protected function processRequest() {
  }

  protected function setTitle() {
  }

  public function setCss() {
    $html = 
      <<<EOD

    <link href='/htdocs/css/fontgoogle1.css' rel='stylesheet' type='text/css'>

    <!--[if lt IE 10]>
        <script type="text/javascript" src="/htdocs/js/media.match.min.js"></script>
        <script type="text/javascript" src="/htdocs/js/placeholder.min.js"></script>
    <![endif]-->

    <link type="text/css" href="/htdocs/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">        <!-- Font Awesome -->
    <link type="text/css" href="/htdocs/css/styles.css" rel="stylesheet">                                     <!-- Core CSS with all styles -->

    <link type="text/css" href="/htdocs/plugins/jstree/dist/themes/avenger/style.min.css" rel="stylesheet">    <!-- jsTree -->
    <link type="text/css" href="/htdocs/plugins/codeprettifier/prettify.css" rel="stylesheet">                <!-- Code Prettifier -->
    <link type="text/css" href="/htdocs/plugins/iCheck/skins/minimal/blue.css" rel="stylesheet">              <!-- iCheck -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
    <!--[if lt IE 9]>
        <link type="text/css" href="/htdocs/css/ie8.css" rel="stylesheet">
        <script type="text/javascript" src="/htdocs/js/cloud.js"></script>
        <script type="text/javascript" src="/htdocs/plugins/charts-flot/excanvas.min.js"></script>
        <script type="text/javascript" src="/htdocs/js/html5.js"></script>
    <![endif]-->

    <!-- The following CSS are included as plugins and can be removed if unused-->
<link type="text/css" href="/htdocs/plugins/datatables/dataTables.css" rel="stylesheet"/>
<link type="text/css" href="/htdocs/plugins/datatables/ColReorder/css/dataTables.colReorder.css" rel="stylesheet"/>
<link type="text/css" href="/htdocs/plugins/datatables/KeyTable/css/dataTables.keyTable.css" rel="stylesheet"/>
<link type="text/css" href="/htdocs/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet"/>
<link type="text/css" href="/htdocs/plugins/datatables/dataTables.fontAwesome.css" rel="stylesheet"/>
    
<link type="text/css" href="/htdocs/plugins/form-daterangepicker/daterangepicker-bs3.css" rel="stylesheet"> 	<!-- DateRangePicker -->
<link type="text/css" href="/htdocs/plugins/fullcalendar/fullcalendar.css" rel="stylesheet"> 					<!-- FullCalendar -->
<link type="text/css" href="/htdocs/plugins/charts-chartistjs/chartist.min.css" rel="stylesheet"> 				<!-- Chartist -->

<link type="text/css" href="/htdocs/css/layout.css" rel="stylesheet"> 				<!-- Chartist -->
EOD;
    if (!empty($this->css)) {
      foreach ($this->css as $css) {
       $html .= '<link rel="stylesheet" type="text/css"'.
         'href="/htdocs/css/'.$css.'.css"/>'; 
      }
    } 
    return $html;
  }


  public function setJs() {
    $html = 
      <<<EOD
<!-- /Switcher -->
    <!-- Load site level scripts -->

<script type="text/javascript" src="/htdocs/js/jquery-1.10.2.min.js"></script> 							<!-- Load jQuery -->
<script type="text/javascript" src="/htdocs/js/jqueryui-1.9.2.min.js"></script> 							<!-- Load jQueryUI -->

<script type="text/javascript" src="/htdocs/js/bootstrap.min.js"></script> 								<!-- Load Bootstrap -->

<script type="text/javascript" src="/htdocs/plugins/easypiechart/jquery.easypiechart.js"></script> 		<!-- EasyPieChart-->
<script type="text/javascript" src="/htdocs/plugins/sparklines/jquery.sparklines.min.js"></script>  		<!-- Sparkline -->
<!--<script type="text/javascript" src="/htdocs/plugins/jstree/dist/jstree.min.js"></script>  				jsTree -->

<script type="text/javascript" src="/htdocs/plugins/codeprettifier/prettify.js"></script> 				<!-- Code Prettifier  -->
<script type="text/javascript" src="/htdocs/plugins/bootstrap-switch/bootstrap-switch.js"></script> 		<!-- Swith/Toggle Button -->

<script type="text/javascript" src="/htdocs/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->
<script type="text/javascript" src="/htdocs/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>  <!-- Bootstrap Tabdrop -->
<script type="text/javascript" src="/htdocs/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.js"></script>  <!-- Bootstrap Tabdrop -->

<script type="text/javascript" src="/htdocs/plugins/iCheck/icheck.min.js"></script>     					<!-- iCheck -->

<script type="text/javascript" src="/htdocs/js/enquire.min.js"></script> 									<!-- Enquire for Responsiveness -->

<script type="text/javascript" src="/htdocs/plugins/bootbox/bootbox.js"></script>							<!-- Bootbox -->

<script type="text/javascript" src="/htdocs/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->

<script type="text/javascript" src="/htdocs/plugins/jquery-mousewheel/jquery.mousewheel.min.js"></script> 	<!-- Mousewheel support needed for jScrollPane -->

<script type="text/javascript" src="/htdocs/js/application.js"></script>
<script type="text/javascript" src="/htdocs/demo/demo-switcher.js"></script>

<!-- End loading site level scripts -->
    
    <!-- Load page level scripts-->
    
<script type="text/javascript" src="/htdocs/plugins/fullcalendar/fullcalendar.min.js"></script>   <!--FullCalendar -->

<script type="text/javascript" src="/htdocs/plugins/wijets/wijets.js"></script>     								<!-- Wijet -->

<script type="text/javascript" src="/htdocs/plugins/charts-chartistjs/chartist.min.js"></script>               	<!-- Chartist -->
<script type="text/javascript" src="/htdocs/plugins/charts-chartistjs/chartist-plugin-tooltip.js"></script>    	<!-- Chartist -->

<script type="text/javascript" src="/htdocs/plugins/form-daterangepicker/moment.min.js"></script>              	<!-- Moment.js for Date Range Picker -->
<script type="text/javascript" src="/htdocs/plugins/form-daterangepicker/daterangepicker.js"></script>     	<!-- Date Range Picker -->

<script type="text/javascript" src="/htdocs/demo/demo-index.js"></script> 										<!-- Initialize scripts for this page-->
<!-- <script type="text/javascript" src="/htdocs/js/Chart.min.js"></script> 										 Initialize scripts for this page-->
<script type="text/javascript" src="/htdocs/js/utils.js"></script>
<script type="text/javascript" src="/htdocs/js/jquery-mask.js"></script>
<script src="/htdocs/plugins/charts-flot/jquery.flot.min.js"></script>            
<script src="/htdocs/plugins/charts-flot/jquery.flot.stack.min.js"></script>    
<script src="/htdocs/plugins/charts-flot/jquery.flot.pie.min.js"></script>      
<script src="/htdocs/plugins/charts-flot/jquery.flot.orderBars.min.js"></script>
<script src="/htdocs/plugins/charts-flot/jquery.flot.resize.js"></script>       
<script src="/htdocs/plugins/charts-flot/jquery.flot.tooltip.min.js"></script>  
<script type="text/javascript" src="/htdocs/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/htdocs/plugins/datatables/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/htdocs/plugins/datatables/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/htdocs/plugins/datatables/KeyTable/js/dataTables.keyTable.min.js"></script>
<script type="text/javascript" src="/htdocs/plugins/datatables/dataTables.bootstrap.js"></script>
EOD;

    if (!empty($this->js)) {
      foreach ($this->js as $js) {
       $html .= '<script type="text/javascript"'.
         'src="/htdocs/js/'.$js.'.js"></script>'; 
      }
    } 
    return $html;
  }

  protected function setHead() {
    $r = $GLOBALS['head'];
    $GLOBALS['head'] = '';
    return $r;
  }

  protected function getRequest() {
    if ($this->request) {
     return $this->request;
    }
  }
}
