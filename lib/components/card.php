<?php
class :d:card extends :x:element {
	attribute
		string tamanho = '3', string titulo, string cor = 'success',
		string subTitulo, string central, string rodape, string classe;
	
	protected function render() {
		$this->tamanho = $this->getAttribute('tamanho');
		$this->titulo = $this->getAttribute('titulo');
		$this->subTitulo = $this->getAttribute('subTitulo');
		$this->central = $this->getAttribute('central');
		$this->rodape = $this->getAttribute('rodape');
		$this->cor = $this->getAttribute('cor');
		$this->classe = $this->getAttribute('classe');
		return
			<x:frag>
				<div class={"col-md-".$this->tamanho." ".$this->classe}>
					<div class={"amazo-tile tile-".$this->cor}>
						<div class="tile-heading">
							<div class="title">
								{$this->titulo}
							</div>
							<div class="secondary">
								{$this->subTitulo}
							</div>
						</div>
						<div class="tile-body">
							<div class="content">
								<label>
									{$this->central}
									</label>
							</div>
						</div>
						<div class="tile-footer">
							<span class="info-text text-right">
								<b>{$this->rodape}</b>
								<!--<i class="fa fa-level-up"></i>-->
							</span>
							<!--<div id="sparkline-revenue" class="sparkline-line">
								<canvas 
									width="226" 
									height="32" 
									style="display: 
										inline-block; width: 226px; 
										height: 32px; vertical-align: top;">
								</canvas>
						</div>-->
					</div>
				</div>
			</div>
			</x:frag>;
	}
}
