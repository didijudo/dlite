<?php
final class :d:colorbox extends :x:element {
	attribute
		string titulo, string icon, string link = "#", string central, 
		string cor = "midnightblue", string tamanho = "6";

	public function render() {
		$this->titulo = $this->getAttribute('titulo');
		$this->icon = $this->getAttribute('icon');
		$this->link = $this->getAttribute('link');
		$this->central = $this->getAttribute('central');
		$this->cor = $this->getAttribute('cor');
		$this->tamanho = $this->getAttribute('tamanho');

		/*switch ($this->cor) {
			case 'laranja':
				$this->cor = 'alizarin';
				break;
			case 'azul':
				$this->cor = 'indigo';
				break;
			case 'azul2':
				$this->cor = 'info';
				break;
			case 'verde':
				$this->cor = 'success';
				break;
			case 'verde2':
				$this->cor = 'grape';
		}*/
		return
			<x:frag>
				<div class={"col-md-".$this->tamanho}>
					<a class={"info-tile has-footer tile-".$this->cor} 
						href={$this->link}>
						<div class="tile-heading">
							<div class="pull-left">
								{$this->titulo}
							</div>
						</div>
						<div class="tile-body">
							<div class="pull-left">
								<i class={"fa fa-".$this->icon}></i>
							</div>
							<div class="pull-right">
								{$this->central}
							</div>
						</div>
						<div class="tile-footer">
							<div class="pull-left">
								Ver programas
							</div>
							<div class="pull-right percent-change">
								<i class="fa fa-arrow-right"></i>
							</div>
						</div>
					</a>
				</div>
			</x:frag>;
	}
}
