<?php
class :d:input extends :x:element {

	attribute
		string name, string type, string placeholder, string icon,
		string class, string id, string value, string tam = 12;
	public function render() {
		$this->name = $this->getAttribute('name');
		$this->tam = $this->getAttribute('tam');
	 	$this->type = $this->getAttribute('type');
	 	$this->icon = $this->getAttribute('icon');
	 	$this->class = $this->getAttribute('class');
	 	$this->id = $this->getAttribute('id');
	 	$this->placeholder = $this->getAttribute('placeholder');
	 	$this->value = $this->getAttribute('value');

		if (!$this->type) {
			$this->type = 'text';
		}

		return
			<x:frag>
				<div class="form-group">
					<div class={"col-xs-".$this->tam}>
						<div class="input-group">							
							<span class="input-group-addon">
								<i class={"fa fa-".$this->icon}></i>
							</span>
							<input type={$this->type} id={$this->id}
								name={$this->name}
								class={"form-control ".$this->class}
								placeholder={$this->placeholder} value={$this->value}/>
						</div>
					</div>
					{$this->getChildren()}
				</div>
			</x:frag>;
	}
}

class :d:select extends :x:element {
	
	attribute
		string name, string type, string placeholder, string icon,
		string class, string id, string classOut, string idParent;	
	public function render() {
		$this->name = $this->getAttribute('name');
	 	$this->type = $this->getAttribute('type');
	 	$this->icon = $this->getAttribute('icon');
	 	$this->class = $this->getAttribute('class');
	 	$this->id = $this->getAttribute('id');
	 	$this->placeholder = $this->getAttribute('placeholder');
	 	$this->classOut = $this->getAttribute('classOut');
	 	$this->idParent = $this->getAttribute('idParent');

		return
			<x:frag>
				<div class={"form-group ".$this->classOut} id={$this->idParent}>
					<div class="col-xs-12">
						<div class="input-group">							
							<span class="input-group-addon">
								<i class={"fa fa-".$this->icon}></i>
							</span>
							<select name={$this->name} 
								class={$this->class} id={$this->id}>
								<option> {$this->placeholder} </option>
								{$this->getChildren()}
							</select>
						</div>
					</div>
				</div>
			</x:frag>;
	}
}
