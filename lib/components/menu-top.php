<?php
class :d:menu-top extends :x:element {

	attribute
		string title='Teste';

	protected function render() {
		return
			<x:frag>
				<header id="topnav"
					class="navbar navbar-inverse navbar-fixed-top clearfix" role="banner">

					<span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
						<a data-toggle="tooltips"
				 			data-placement="right" 
							title="Toggle Sidebar">
						<span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a>
					</span>
					<a class="navbar-brand" href="/home">Finanças</a>

					<!--<span id="trigger-infobar" class="toolbar-trigger toolbar-icon-bg">
						<a data-toggle="tooltips" 
							data-placement="left" 
							title="Toggle Infobar">
							<span class="icon-bg">
								<i class="fa fa-fw fa-bars"></i>
							</span>
						</a>
					</span>-->
	
					<ul class="nav navbar-nav toolbar pull-right">
						<li class="dropdown toolbar-icon-bg">
							<a href="#" id="navbar-links-toggle" data-toggle="collapse" data-target="header>.navbar-collapse">
								<span class="icon-bg">
									<i class="fa fa-fw fa-ellipsis-h"></i>
								</span>
							</a>
						</li>

						<!--<li class="toolbar-icon-bg demo-headerdrop-hidden">
							<a href="#" id="headerbardropdown">
								<span class="icon-bg">
									<i class="fa fa-fw fa-level-down"></i>
									</span>
							</a>
						</li>-->

						<li class="dropdown toolbar-icon-bg">
							<a href="#" class="dropdown-toggle prepend-right" data-toggle="dropdown">
								<span class="icon-bg"><i class="fa fa-fw fa-user"></i></span>
							</a>
							<ul class="dropdown-menu userinfo arrow">
								<li>
									<a href="/alterar/usuario">
										<span class="pull-left">
											Alterar Usuario
										</span> 
										<i class="pull-right fa fa-user"></i>
									</a>
								</li>
								<li>
									<a href="/alterar/usuario/senha">
										<span class="pull-left">
											Alterar Senha
										</span> 
										<i class="pull-right fa fa-key"></i>
									</a>
								</li>
								<li>
									<a href="/logout">
										<span class="pull-left">
											 Sair</span> 
										<i class="pull-right fa fa-sign-out"></i>
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</header>
			</x:frag>;
	}
}
