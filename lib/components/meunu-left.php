<?php
class :d:menu-left extends :x:element {

	protected function render() {
		$admin = 
			<x:frag>
				<li>
					<a href="/extrato">
						<i class="fa fa-money"></i>
						<span> Extrato </span>
					</a>
				</li>
				<li>
					<a href="/conta/banco">
						<i class="fa fa-bank"></i>
						<span> Contas por Banco </span>
					</a>
				</li>
				<li>
					<a href="javascript:;">
						<i class="fa fa-plus"></i>
						<span>Novo</span>
					</a>
					<ul class="acc-menu">
						<li><a href="/cadastrar/secretaria">Secretaria</a></li>
						<li><a href="/cadastrar/programa">Programa</a></li>
						<li><a href="/cadastrar/banco">Banco</a></li>
						<li><a href="/cadastrar/conta">Conta</a></li>
						<li><a href="/cadastrar/custos">Centro de Custo</a></li>
						<li><a href="/cadastrar/fornecedor">Fornecedor</a></li>
						<li><a href="/cadastrar/imposto">Imposto</a></li>
					</ul>
				</li>
				<li>
					<a href="javascript:;">
						<i class="fa fa-exchange"></i>
						<span>Movimentacao</span>
					</a>
					<ul class="acc-menu">
						<li>
							<a href="/movimentacao/entrada">Entrada</a>
						</li>
						<li>
							<a href="/movimentacao/saida">Saida</a>
						</li>
					</ul>
				</li>
			<!--	<li>
					<a href="javascript:;">
						<i class="fa fa-pencil"></i>
						<span>Alterar</span>
					</a>
					<ul class="acc-menu">
						<li>
							<a href="/alterar/conta">Alterar conta</a>
						</li>
						<li>
							<a href="/alterar/usuario/perfil">Perfil do usuário</a>
						</li>
					</ul>
				</li>-->
				<li>
					<a href="/listar/usuarios">
						<i class="fa fa-users"></i>
						<span> Listar usuários </span>
					</a>
				</li>
			</x:frag>;
		return
			<x:frag>
				<div class="static-sidebar-wrapper sidebar-midnightblue">
					<div class="static-sidebar">
						<div class="sidebar">
							<div class="widget stay-on-collapse" id="widget-welcomebox">
								<div class="widget-body welcome-box tabular">
									<div class="tabular-row">
										<div class="tabular-cell welcome-options">
											<span class="welcome-text">Bem Vindo,</span>
											<a href="#" class="name"> {$_SESSION['usuario']->nome}</a>
										</div>
									</div>
								</div>
							</div>
							<div class="widget stay-on-collapse" id="widget-sidebar">
								<nav role="navigation" class="widget-body">
									<ul class="acc-menu">
										<li class="nav-separator">Menu</li>
										<li>
											<a href="/home">
												<i class="fa fa-home"></i><span>Home</span>
											</a>
										</li>
										{$admin}
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</x:frag>;
	}
}
