<?php
class :d:painel extends :x:element {
	attribute
		string titulo, string botao = 'Ok!', string idBtn,
		bool close = true;

	protected function render() {
		$this->botao = $this->getAttribute('botao');
		$this->titulo = $this->getAttribute('titulo');
		$this->idBtn = $this->getAttribute('idBtn');
		$this->close = $this->getAttribute('close');
		if ($this->close) {
			$close = 
					<button 
						type="button" 
						class="close" 
						data-dismiss="alert" aria-hidden="true">
							×
					</button>;
		} else {
			$close;
		}
		return
			<x:frag>
				<div class="alert alert-dismissable alert-info">
					{$close}
					<h3>{$this->titulo}</h3> 
					{$this->getChildren()}
					<a id={$this->idBtn} 
						class="btn btn-info prepend-top" 
						data-dismiss="alert" href="#">
						{$this->botao}
					</a>
				</div>
			</x:frag>;
	}

}
