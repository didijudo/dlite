<?php
final class :d:perfil extends :x:element {

	public function render() {
		return
			<x:frag>
				<d:form action="#" method="post" title="Perfil">
					<d:input id="idNome"
						name="nome" 
						placeholder="Nome do perfil" icon="dashboard"/>
						<fieldset>
							<legend>Acesso do perfil</legend>
							<div data-row-span="2">
								<div data-field-span="1">
									<b>Movimentação</b><br/>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/movimentacao/entrada"/>
										Entrada &nbsp;
									</label>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/movimentacao/saida"/>
										 Saida
									</label>
								</div>

								<div data-field-span="1">
									<b>Cadastrar</b><br/>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/cadastrar/usuario"/>
										Usuario &nbsp;
									</label>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/cadastrar/programa"/>
										 Programa &nbsp;
									</label>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/cadastrar/conta"/>
										 Conta &nbsp;
									</label>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/cadastrar/secretaria"/>
										 Secretaria &nbsp;
									</label>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/cadastrar/banco"/>
										Banco &nbsp;
									</label>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/cadastrar/fornecedor"/>
										Fornecedor &nbsp;
									</label>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/cadastrar/custos"/>
										Centro de Custo &nbsp;
									</label>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/cadastrar/imposto"/>
										Impostos &nbsp;
									</label>
								</div>

								<div data-field-span="1">
									<b>Extratos</b><br/>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/extrato"/>
										Extrato &nbsp;
									</label>
									<!--<label>
										<input 
											type="checkbox" 
											name="url[]" value="/programa/detalhes"/>
										Extrato Programas &nbsp;
									</label>-->
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/secretaria/detalhes"/>
										Extrato Secretaria &nbsp;
									</label>
								</div>

								<div data-field-span="1">
									<b>Gestão de usuários</b><br/>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/perfil/criar"/>
										Criar perfil &nbsp;
									</label>
									<label>
										<input 
											type="checkbox" 
											name="url[]" value="/gestor"/>
										Alterar gestor &nbsp;
									</label>
								</div>
							</div>
						</fieldset>
				</d:form>
			</x:frag>;
	}
}
