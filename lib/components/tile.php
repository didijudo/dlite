<?php
final class :d:tile extends :x:element {
	attribute
		string tamanho = 3, string classe, string centro, string titulo;

	public function render() {
		$this->tamanho = $this->getAttribute('tamanho');
		$this->classe = $this->getAttribute('classe');
		$this->centro = $this->getAttribute('centro');
		$this->titulo = $this->getAttribute('titulo');
		return
			<x:frag>
				<div class={"col-md-".$this->tamanho." ".$this->classe}>
					<a class="info-tile tile-sky" href="#">
						<div class="tile-heading">
							<div class="pull-left">
								{$this->titulo}
							</div>
							<div class="pull-right">
							
							</div>
						</div>
						<div class="tile-body">
							<div class="pull-left"><i class="fa fa-dollar"></i></div>
							<div class="pull-right">
								<span style="font-size: 22px;">{$this->centro}</span>
							</div>
						</div>
					</a>
				</div>
			</x:frag>;
	}
}
