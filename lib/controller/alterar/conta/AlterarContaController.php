<?php
final class AlterarContaController extends LayoutController {
	public $js = array('alterarConta');
	private $conta;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Alterar Conta </h1>
				</div>
				<d:form title="Alterar Conta" action="#" method="post"
					lblButton="Alterar">
					<d:input name="nome" value={$this->conta->nome} icon="asterisk"/>
					<d:input name="agencia" value={$this->conta->agencia} icon="home"/>
					<d:input name="numero" value={$this->conta->numero} icon="envelope"/>
					<input type="hidden" name="id" value={$this->conta->id}/>
				</d:form>
			</x:frag>;
	}

	public function processRequest() {
		Gandalf::check();
		$r = $this->getRequest();
		$conn = dbconn();

		$conta_id = $r->getInt('id');
		$q =
			<<<EOD
SELECT 
	c.contaNome as nome,
	c.contaID as id,
	c.contaAgencia as agencia,
	c.contaNumero as numero,
	s.secretariaID as secretaria
FROM contas c, secretaria s, programa p
WHERE contaID = $conta_id
	AND p.programaID = c.programaID
	AND s.secretariaID = p.secretariaID;
EOD;
		$res = $conn->executeQuery($q);
		$this->conta = mysql_fetch_object($res);

		if (is_post()) {
			$nome = $r->getString('nome');
			$agencia = $r->getString('agencia');
			$numero = $r->getString('numero');
			$id = $r->getInt('id');	

			if (!$nome) {
				add_msg(ERROR, 'Digite o nome da conta');
				return;
			}
			if (!$agencia) {
				add_msg(ERROR, 'Digite o número da agencia');
				return;
			}
			if (!$numero) {
				add_msg(ERROR, 'Digite o número da conta');
				return;
			}

			$q_update =
				<<<EOD
UPDATE contas SET 
	contaNome = '$nome',
	contaNumero = '$numero',
	contaAgencia = '$agencia'
WHERE contaID = $id;
EOD;
			$res = $conn->executeQuery($q_update);

			if ($res) {
				add_msg(SUCCESS, 'Conta alterada com sucesso');
				send_redirect('/secretaria/detalhes?id='.$this->conta->secretaria);
			} else {
				add_msg(ERROR, 'Erro ao alterar a conta');
				return;
			}
		}
	}
}
