<?php
final class AlterarProgramaController extends LayoutController {
	private $selectConta, $selectSec, $nome, $id;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Alterar Programa </h1>
				</div>
				<d:form action="" method="post" lblButton="Alterar">
					<d:input 
						id="idNome"
						type="text" 
						name="nome" 
						icon="book" 
						value={$this->nome}
						placeholder="Nome Programa"/>	

					{$this->selectSec}
					<input type="hidden" name="programa" value={$this->id}/>
				</d:form>
			</x:frag>;
	}

	public function processRequest() {
		Gandalf::check();
		js_call("$('#idNome').maiusculo();");

		$r = $this->getRequest();
		$id = ($r->getInt('id'))? $r->getInt('id') : $r->getInt('programa');
		$conn = dbconn();
		$q1 =
			<<<EOD
SELECT 
	s.secretariaNome as nome,
	s.secretariaID as id
FROM
	secretaria s;
EOD;

		$q_sel =
			<<<EOD
SELECT
	programaNome as nome,
	programaID as id,
	secretariaID as secretaria
FROM programa
WHERE programaID = $id;
EOD;

		$res_sel = $conn->executeQuery($q_sel);
		$programa = mysql_fetch_object($res_sel);
		$this->nome = $programa->nome;
		$this->id = $programa->id;

		$this->selectSec = 
			<d:select name="secretaria" class="form-control" icon="briefcase" placeholder="Selecione uma secretaria"/>;
		$res1 = $conn->executeQuery($q1);
		while($o = mysql_fetch_object($res1)) {
			$selected = 'false';
			if ($o->id == $programa->secretaria) {
				$selected = 'true';
			}
			$this->selectSec->appendChild(
				<option value={$o->id} selected={$selected}> {$o->nome} </option>
			);	
		} 
	
		if (is_post()) {
			$secretaria = $r->getInt('secretaria');
			$id = $r->getString('programa');
			$nome = $r->getString('nome');

			if (!$nome) {
				add_msg(ERROR, 'Digite um nome para o programa');
				send_redirect('/alterar/programa?id='.$this->id);
			}

			if (!$secretaria) {
				add_msg(ERROR, 'Escolha uma secretaria');
				send_redirect('/alterar/programa?id='.$this->id);
			}

			$q_update =
				<<<EOD
UPDATE programa SET programaNome = '$nome', secretariaID = $secretaria 
WHERE programaID = $id;
EOD;
			$res_update = $conn->executeQuery($q_update);

			if ($res_update) {
				add_msg(SUCCESS, 'Programa alterado com sucesso');
				send_redirect('/secretaria/detalhes?id='.$secretaria);
			} else {
				add_msg(ERROR, 'Não foi possível alterar neste momento');
				send_redirect('/alterar/programa?id='.$this->id);
			}
		}
	}
}
