<?php
final class AlterarUsuarioController extends LayoutController {
	private $nome;
	private $email;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Alterar Usuário </h1>
				</div>
				<div class="col-xs-12">
					<d:form action="#" title="" method="post" lblButton="Alterar">
						<d:input name="nome" value={$this->nome} id="idNome" icon="user"/>
						<d:input name="email" value={$this->email} id="idEmail" icon="at"/>
					</d:form>
					
				</div>
			</x:frag>;
	}

	public function processRequest() {
		Gandalf::needLogin();
		js_call("$('#idNome').maiusculo();");
		$r = $this->getRequest();
		$this->nome = Gandalf::getUsuario()->nome;
		$this->email = Gandalf::getUsuario()->email;

		if (is_post()) {
			$nome = $r->getString('nome');
			$email = $r->getString('email');
			$usuario_id = Gandalf::getUsuario()->id;

			if ($nome == $this->nome && $email == $this->email) {
				add_msg(WARNING, 'Nada foi alterado');
				return;
			}
			$q =
				<<<EOD
UPDATE usuarios 
	SET usuarioNome = '$nome', 
	usuarioEmail = '$email'
WHERE usuarioID = $usuario_id;
EOD;
			$conn = dbconn();
			$res = $conn->executeQuery($q);
			if ($res) {
				$q_sel =
					<<<EOD
SELECT 
	usuarioID as id, 
	usuarioNome as nome, 
	usuarioEmail as email,
	usuarioBloqueio as bloqueado,
	usuarioNivel as nivel,
	usuarioAcesso as acesso
FROM usuarios 
WHERE usuarioID = $usuario_id;
EOD;
				$res_sel = $conn->executeQuery($q_sel);
				if ($res_sel) {
					$o = mysql_fetch_object($res_sel);
					$_SESSION['usuario'] = $o;
					add_msg(SUCCESS, 'Usuário alterado com sucesso');
					$this->nome = $nome;
					$this->email = $email;
				} else {
					add_msg(ERROR, 'Erro ao alterar o usuário.');
				}
			}
		}
	}
}
