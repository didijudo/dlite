<?php
final class AlterarUsuarioAdminController extends LayoutController {
	private $nome;
	private $email;
	private $perfil;
	private $id;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Alterar Usuário </h1>
				</div>
				<div class="col-xs-12">
					<d:form action="#" title="" method="post" lblButton="Alterar">
						<d:input name="nome" value={$this->nome} id="idNome" icon="user"/>
						<d:input name="email" value={$this->email} id="idEmail" icon="at"/>
						{$this->perfil}
						<input type="hidden" name="id" value={$this->id}/>
					</d:form>
					
				</div>
			</x:frag>;
	}

	public function processRequest() {
		Gandalf::check();
		$r = $this->getRequest();
		$id = $r->getInt('id');
		$this->id = $id;

		$q =
			<<<EOD
SELECT 
	u.usuarioNome as nome,
	u.usuarioID as id,
	u.usuarioEmail as email,
	p.perfilID as perfil
FROM usuarios u, perfil p
WHERE u.usuarioID = $id
	AND u.usuarioNivel = p.perfilID;
EOD;

		$conn = dbconn();
		$res = $conn->executeQuery($q);
		$this->perfil = <d:select id="" class="form-control" name="perfil" icon="dashboard"/>;
		if ($res) {
			$o = mysql_fetch_object($res);

			$q_perfil =
				<<<EOD
SELECT perfilID as id, perfilNome as nome FROM perfil;
EOD;

			$res_perfil = $conn->executeQuery($q_perfil);
			while ($p = mysql_fetch_object($res_perfil)) {
				$selected = 'false';
				if ($p->id == $o->perfil) {
					$selected = 'true';
				}
				$this->perfil->appendChild(
					<option value={$p->id} selected={$selected}> {$p->nome}</option>
				);
			}
			$this->nome = $o->nome;
			$this->email = $o->email;
		}

		if (is_post()) {
			$nome = $r->getString('nome');
			$email = $r->getString('email');
			$perfil = $r->getInt('perfil');
			$this->id = $r->getInt('id');

			if (!$nome) {
				add_msg(ERROR, 'Um nome precisa ser preenchido');
				return;
			}

			if (!$email) {
				add_msg(ERROR, 'Um email precisa ser preenchido');
				return;
			}

			if (!$perfil) {
				add_msg(ERROR, 'Um perfil precisa ser escolhido');
				return;
			}

			$q_update =
				<<<EOD
UPDATE usuarios 
	SET usuarioNome = '$nome', usuarioEmail='$email', usuarioNivel=$perfil
WHERE usuarioID = $this->id;
EOD;
			$res_update = $conn->executeQuery($q_update);
			if ($res_update) {
				add_msg(SUCCESS, 'Usuário alterado com sucesso');
				send_redirect('/listar/usuarios');
			}
		}
	}
}
