<?php
final class ExcluirUsuarioController extends Controller {

	public function processRequest() {
		$id = $this->getRequest()->getInt('id');	

		if (is_post()) {
			$q =
				<<<EOD
UPDATE usuarios SET usuarioBloqueio = 2 WHERE usuarioID = $id;
EOD;
			$res = dbconn()->executeQuery($q);
			if ($res) {
				add_msg(SUCCESS, 'Usuário excluído com sucesso');
				send_redirect('/listar/usuarios');
			} else {
				add_msg(ERROR, 'Erro ao excluir usuário');
				send_redirect('/listar/usuarios');
			}
		}
	}
}
