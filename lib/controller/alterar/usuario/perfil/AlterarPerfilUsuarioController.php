<?php
final class AlterarPerfilUsuarioController extends LayoutController {
	private $selectPerfil;
	private $selectUsuario;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Alterar perfil do Usuário</h1>
				</div>
				<d:form action="#" method="post" lblButton="Alterar">
					{$this->selectUsuario}	
					{$this->selectPerfil}
				</d:form>
			</x:frag>;
	}

	public function processRequest() {
		Gandalf::check();
		$conn = dbconn();
		$q_usuario = 
			<<<EOD
SELECT 
	usuarioNome as nome,
	usuarioID as id
FROM usuarios
EOD;
		$this->selectUsuario = 
			<d:select name="usuario"
				class="form-control" placeholder="Usuario" icon="male"/>;
		$result_usu = $conn->executeQuery($q_usuario);
		while($o = mysql_fetch_object($result_usu)) {
			$this->selectUsuario->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);
		}

		$q0 =
			<<<EOD
SELECT 
	perfilID as id,
	perfilNome as nome
FROM perfil
EOD;
		$this->selectPerfil = 
			<d:select name="perfil"
				class="form-control" placeholder="Perfil do usuario" icon="gears"/>;
		$result = $conn->executeQuery($q0);
		while($o = mysql_fetch_object($result)) {
			$this->selectPerfil->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);
		}
		
		if (is_post()) {
			$r = $this->getRequest();
			$usuario_id = $r->getInt('usuario');
			$perfil_id = $r->getInt('perfil');

			$q_alter =
				<<<EOD
UPDATE usuarios SET usuarioNivel = $perfil_id WHERE usuarioID = $usuario_id;
EOD;
			$res_alter = $conn->executeQuery($q_alter);
			if ($res_alter) {
				add_msg(SUCCESS, 'Perfil do usuario alterado com sucesso');
				return;
			} else {
				add_msg(ERROR, 'Erro ao alterar o perfil. Procure o administrador.');
				return;
			}
		}
	}
}
