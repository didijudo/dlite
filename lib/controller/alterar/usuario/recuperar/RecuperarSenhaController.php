<?php
final class RecuperarSenhaController extends WebController {

	public function composeBody() {
		return
			<x:frag>
				<div class="row">
					{$this->getMsg()}
				</div>
				<div class="row prepend-top">
					<d:form action="#" method="post" lblButton="Enviar">
					<d:input type="email" name="email"
						icon="envelope" placeholder="E-mail"/>
					</d:form>
				</div>
			</x:frag>;
	}

	public function processRequest() {
		$r = $this->getRequest();
		$email = $r->getString('email');

		if (is_post()) {
			if (!$email) {
				add_msg(ERROR, 'Digite um email.');
				return;
			}

			$q =
				<<<EOD
SELECT
	usuarioID as id,
	usuarioEmail as email,
	usuarioNome as nome
FROM usuarios
WHERE usuarioEmail = '$email'
	AND usuarioBloqueio <> 2; 
EOD;
			$conn = dbconn();
			$res = $conn->executeQuery($q);
			if ($res) {
				if (mysql_num_rows($res) == 0) {
					add_msg(ERROR, 'Usuário inexistente ou bloqueado');
					return;
				}
				$user = mysql_fetch_object($res);
				$hash = hash_string(6);	
				$md5 = md5($hash);
				$q_update =
					<<<EOD
UPDATE usuarios SET usuarioSenha = '$md5' WHERE usuarioID = $user->id
EOD;
				$res_update = $conn->executeQuery($q_update);
				if ($res_update) {
					$body =
						<<<EOD
							<div>
								Olá {$user->nome},<br/>
								Você solicitou a recuperação de senha do sistema 
								<a href="www.sefincaninde.com.br" target="_blank"> Financas </a>. <br/>
								Sua nova senha para acesso ao sistema é 
								<a href=""><b>{$hash}</b></a><br/>
								<footer> 
									<h4 style="margin: 0;"> &copy; 2015 Somar Gestão pública</h4>
								</footer>
							</div>
EOD;
					$to = "$user->nome:$user->email";
					$sent = send_mail('Recuperação de senha', $body, $to);
					if ($sent) {
						add_msg(SUCCESS, "Uma nova senha foi enviada para o email ".$user->email);
						send_redirect('/login');
					}
				}
			} else {
				add_msg(ERROR, 'Erro ao resetar a senha');
				return;
			}
		}
	}
}
