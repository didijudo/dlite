<?php
final class AlterarSenhaUsuario extends LayoutController {
	private $usuario;
	
	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Alterar senha </h1>
				</div>
				<d:form action="#" lblButton="Alterar" method="post">
					<d:input type="password" 
						name="atual" id="idNome" placeholder="Senha Atual" icon="key"/>
					<d:input type="password"
						name="nova" id="idNome" placeholder="Nova Senha" icon="key"/>
					<d:input type="password" 
						name="renova" id="idNome" placeholder="Repita a nova" icon="key"/>
				</d:form>
			</x:frag>;
	}


	public function processRequest() {
		if (!Gandalf::isLogged()) {
			send_redirect('/logout');
		}

		if (is_post()) {
			$r = $this->getRequest();
			$atual = $r->getString('atual');
			$nova = $r->getString('nova');
			$renova = $r->getString('renova');

			if ($nova != $renova) {
				add_msg(ERROR, 'As senhas novas não conferem');
				return;
			}
			if (!$atual) {
				add_msg(ERROR, 'Digite a senha atual');
				return;
			}
			if (!$nova) {
				add_msg(ERROR, 'Digite a senha nova');
				return;
			}
			
			$id = $_SESSION['usuario']->id;
			$conn = dbconn();
			$q =
				<<<EOD
SELECT usuarioSenha as senha FROM usuarios WHERE usuarioID = $id;
EOD;
			$res = $conn->executeQuery($q);

			$senhaBanco = mysql_fetch_object($res);

			if ($senhaBanco->senha != md5($atual)) {
				add_msg(ERROR, 'A senha atual não esta correta');
				return;
			}

			$nova = md5($nova);
			$q1 =
				<<<EOD
UPDATE usuarios SET usuarioSenha='$nova' WHERE usuarioID=$id;
EOD;
			$res1 = $conn->executeQuery($q1);

			if ($res1) {
				add_msg(SUCCESS, 'Senha alterada com sucesso');
				return;
			} else {
				add_msg(ERROR, 'Algo errado aconteceu =(');
				return;
			}
		}
	}
}
