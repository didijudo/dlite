<?php
final class CustoAutocompleteController extends RestController {
	private $array;

	public function restResponse() {
		return $this->array;
	}

	public function processRequest() {
		$r = $this->getRequest();
		$nome = $r->getString('nome');
		$tipo = $r->getInt('tipo');
		dlog($tipo);
		$q =
			<<<EOD
SELECT custoID as id, custoNome as label, custoNome as value 
FROM custo 
WHERE custoNome like '%$nome%'
	AND custoTipo = $tipo
EOD;
		$conn = dbconn();
		$res = $conn->executeQuery($q);
		if ($res) {
			while($o = mysql_fetch_object($res)) {
				$o->label = mb_convert_encoding($o->label, 'UTF-8', 'LATIN1');
				$o->value = mb_convert_encoding($o->value, 'UTF-8', 'LATIN1');
				$this->array[] = $o;
			} 
		}
	}
}
