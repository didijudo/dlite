<?php
final class FornecedroAutocompleteController extends RestController {
	private $array;

	public function restResponse() {
		return $this->array;
	}

	public function processRequest() {
		$r = $this->getRequest();
		$nome = $r->getString('nome');
		$q =
			<<<EOD
SELECT fornecedorID as id, fornecedorNome as label, fornecedorNome as value 
FROM fornecedor WHERE fornecedorNome like '%$nome%'
EOD;
		$conn = dbconn();
		$res = $conn->executeQuery($q);
		if ($res) {
			while($o = mysql_fetch_object($res)) {
				$this->array[] = $o;
			} 
		}
	}
}
