<?php
final class ContaPorProgramaController extends RestController {
	private $array;

	public function restResponse() {
		return $this->array;
	}

	public function processRequest() {
		$r = $this->getRequest();
		$programa_id = $r->getInt('programa');
		$q_conta =
			<<<EOD
SELECT 
	contaNome as nome,
	contaID as id,
	contaAgencia as agencia,
	contaNumero as numero
FROM contas
WHERE programaID = $programa_id;
EOD;
		$conn = dbconn();
		$result = $conn->executeQuery($q_conta);
		if ($result) {
			while($o = mysql_fetch_object($result)) {
				$this->array[] = $o;
			}
		}	else {
			$this->array[] = array("Erro" => "Erro na consulta da conta");
		}
	}
}
