<?php
final class ValoresPrefeituraController extends RestController {
	private $array = array();

	public function restResponse() {
		return $this->array;
	}

	public function processRequest() {
		$q = 
			<<<EOD
SELECT SUM(contaSaldo) as total FROM contas;
EOD;
		$conn = dbconn();
		$res = $conn->executeQuery($q);
		$total = mysql_fetch_object($res)->total;

		$q_secretaria =
			<<<EOD
SELECT 
	secretariaNome as nome,
	secretariaID as id,
	secretariaCor as cor,
	1 as percent
FROM 
	secretaria
EOD;

		$this->linha = <div class="col-xs-12"/>;
		$res = $conn->executeQuery($q_secretaria);
		
		while($o = mysql_fetch_object($res)) {
			$q_sum =
				<<<EOD
SELECT 
	SUM(contaSaldo) AS saldo 
FROM contas 
WHERE programaID IN 
	(SELECT programaID FROM programa WHERE secretariaID = $o->id )
EOD;
			$res_sum = $conn->executeQuery($q_sum);
			$saldo = mysql_fetch_object($res_sum)->saldo;

			$o->percent = (($saldo * 100)/ $total);

			switch ($o->cor) {
				case 'sky': 
					$o->cor = '#009688';
					break;
				case 'orange': 
					$o->cor = '#ff9800';
					break;
				case 'brown': 
					$o->cor = '#694a3e';
					break;
				case 'midnightblue': 
					$o->cor = '#37474f';
					break;
				case 'success': 
					$o->cor = '#7eb73d';
					break;
				case 'primary': 
					$o->cor = '#0398db';
					break;
				case 'danger': 
					$o->cor = '#d0181e';
					break;
				case 'magenta': 
					$o->cor = '#d81557';
					break;
				case 'alizarin': 
					$o->cor = '#ff5722';
					break;
			}
			$this->array[] = $o;
		}
	}
}
