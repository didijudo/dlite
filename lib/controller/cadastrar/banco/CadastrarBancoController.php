<?php
final class CadastrarBancoController extends LayoutController {
	private $bancos;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Cadastrar Banco </h1>
				</div>
				<d:form method="post" action="#">
					<d:input id="idNome" name="nome" placeholder="Nome do banco" icon="bank"/>
				</d:form>
				<div class="col-xs-6 col-xs-offset-3">
					<d:accordion head={"Bancos cadastrados"}>
						{$this->bancos}			
					</d:accordion>
				</div>
			</x:frag>;
	}

	public function processRequest() {
		js_call("$('#idNome').maiusculo();");
		Gandalf::check();
		$conn = dbconn();
		$q_bancos =
			<<<EOD
SELECT bancoNome, bancoImg FROM banco ORDER BY bancoNome ASC;
EOD;
		$res_bancos = $conn->executeQuery($q_bancos);
		$this->bancos = <ul class="list-unstyled col-xs-12"/>;

		while($o = mysql_fetch_object($res_bancos)) {
			$this->bancos->appendChild(
				<li>
					<h5>
						<img src={$o->bancoImg} style="padding-right:5px"/> 
						{$o->bancoNome}
					</h5>
				</li>
			);
		}

		if (is_post()) {
			$nome_banco = $this->getRequest()->getString('nome');

			if (!$nome_banco) {
				add_msg(ERROR, 'Digite o nome do banco');
				return;
			}

			$q_sel =
				<<<EOD
SELECT bancoID as id FROM banco WHERE bancoNome like '%$nome_banco%'
EOD;
			$res_sel = $conn->executeQuery($q_sel);
			$result = mysql_fetch_object($res_sel)->id;

			if ($result) {
				add_msg(WARNING, 'Banco já cadastrado');
				return;
			}
			$q =
				<<<EOD
INSERT INTO banco(bancoNome) VALUES('$nome_banco');
EOD;
			$res = $conn->executeQuery($q);

			if ($res) {
				add_msg(SUCCESS, 'Banco cadastrado com sucesso');
			} else {
				add_msg(ERROR, 'Erro ao cadastrar o banco');
				return;
			}
		}
	}
}

