<?php
final class CadastrarContaController extends LayoutController {
	public $js = array('cadastrarConta');
	private $select;
	private $selectPrograma;
	private $secretaria;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Cadastro de Conta </h1>
				</div>
				<d:form action="#" method="post" lblButton="Cadastrar">
						
					{$this->select}

					<d:input 
						type="text" 
						id="idAgencia"
						name="agencia"
						placeholder="Agencia"
						icon="home"/>

					<d:input 
						type="text" 
						name="conta"
						placeholder="Numero da Conta"
						icon="envelope"/>

					<d:input 
						id="idNome"
						type="text" 
						name="nome"
						placeholder="Nome da conta"
						icon="asterisk"/>

					{$this->selectPrograma}
					{$this->secretaria}
				</d:form>
			</x:frag>;	
	}

	public function processRequest() {
		Gandalf::check();
		js_call("$('#idNome').maiusculo();");
		//js_call("$('#idAgencia').mask('')");
		$conn = dbconn();
		$q =
			<<<EOD
SELECT * FROM banco;
EOD;

		$q1 =
			<<<EOD
SELECT programaID as id, programaNome as nome FROM programa
EOD;

		$this->select = <d:select	name="banco" class="form-control" icon="bank" placeholder="Selecione o banco"/>;
		$this->selectPrograma =
			<d:select
				id="idPrograma"
				name="programa"
				class="form-control"
				icon="briefcase"
				placeholder="Selecione o programa"/>;

		$res = $conn->executeQuery($q);
		while($o = mysql_fetch_object($res)) {
			$this->select->appendChild(
				<option value={$o->bancoID}> {$o->bancoNome} </option>
			);
		} 

		$q_sec =
			<<<EOD
SELECT 
	secretariaNome as nome,
	secretariaID as id
FROM secretaria;
EOD;
		$res = $conn->executeQuery($q_sec);
		$this->secretaria = 
			<d:select class="form-control"
				classOut="hidden"
				idParent="idPai"
				icon="folder" 
				name="secretaria"
				id="idSecretaria"
				placeholder="Selecione a secretaria"/>;

		while($o = mysql_fetch_object($res)) {
			$this->secretaria->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);
		}

	//	$this->selectPrograma->appendChild(<option value="-1">Sem vinculo com programa</option>);
		$res2 = $conn->executeQuery($q1);
		while($o = mysql_fetch_object($res2)) {
			$this->selectPrograma->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);
		} 


		if (is_post()) {
			$r = $this->getRequest();
			$banco = $r->getInt('banco');
			$agencia = $r->getString('agencia');
			$conta = $r->getString('conta');
			$nome = $r->getString('nome');
			$id_programa = $r->getInt('programa');
			$secretaria_id = $r->getInt('secretaria');

			if ($id_programa == -1) {
				if (!$secretaria_id) {
					add_msg(ERROR, 
						'Escolha uma secretaria para vincular uma conta sem programa');
					return;
				}	
			} else {
				$secretaria_id = 'null';
			}

			if ($this->existe_conta($agencia, $conta, $conn)) {
				add_msg(ERROR, 'Essa conta já foi cadastrada');
				return;
			}

			//Guardas
			if (!$banco) {
				add_msg(ERROR, 'Escolha um banco');
				return;
			}
			if (!$agencia) {
				add_msg(ERROR, 'Digite uma agência');
				return;
			}
			if (!$conta) {
				add_msg(ERROR, 'Digite uma conta');
				return;
			}

			if (!$id_programa) {
				$id_programa = 'null';
			}

			$q =
				<<<EOD
INSERT INTO contas(contaNome, bancoID, contaAgencia, contaNumero, contaSaldo, programaID, secretariaID) 
VALUES('$nome', $banco, '$agencia', '$conta', 0, $id_programa, $secretaria_id);
EOD;
			$res = $conn->executeQuery($q);
			if ($res) {
				add_msg(SUCCESS, "Conta cadastrada com sucesso");
				return;
			} else {
				add_msg(ERROR, "Algo errado aconteceu ");
				return;
			}
		}
	}

	private function existe_conta($agencia, $conta, $conn) {
		$q =
			<<<EOD
SELECT 1 FROM contas WHERE contaAgencia='$agencia' AND contaNumero='$conta';
EOD;
		$res = $conn->executeQuery($q);
		if (mysql_fetch_object($res)) {
			return true;
		} else {
			return false;
		}
	}
}
