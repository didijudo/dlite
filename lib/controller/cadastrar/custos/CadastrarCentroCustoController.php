<?php
final class CadastrarCentroCustoController extends LayoutController {
	public $js = array('custos');
	private $custos;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Cadastrar Centro de Custos </h1>
				</div>
				<d:form method="post" action="#">
					<d:input 
						id="idCusto" 
						name="custoNome" 
						placeholder="Nome do centro de custos" 
						icon="gear"/>
					{$this->selectCentros}
				</d:form>
				<div class="col-xs-6 col-xs-offset-3">
					<d:accordion head={"Centros de Custos cadastrados"}>
						{$this->centros}			
					</d:accordion>
				</div>
			</x:frag>;
	
	}

	public function processRequest() {
    js_call("$('#idCusto').maiusculo();");
		$conn = dbconn();
		$q_centros =
			<<<EOD
SELECT custoID as id, custoNome as nome FROM custo ORDER BY custoNome ASC;
EOD;
		$res_centros = $conn->executeQuery($q_centros);
		$this->centros = <ul class="list-unstyled col-xs-12"/>;

		while($o = mysql_fetch_object($res_centros)) {
			$o->nome = mb_convert_encoding($o->nome, 'UTF-8', 'LATIN1');
			$link =
				<x:frag>
					<a onclick={"excluir(".$o->id.")"}
						class="btn btn-danger btn-xs prepend-left pull-right" title="Remover">
						<i class="fa fa-trash"/>
					</a>
					<hr/>
				</x:frag>;
			$this->centros->appendChild(
				<li class=""> 
					<h6>
						{$o->nome}
						<span>
							{$link}
						</span>
					</h6>
				</li>
			);
		}

		if (is_post()) {
			$nome_centro = $this->getRequest()->getString('custoNome');

			if (!$nome_centro) {
				add_msg(ERROR, 'Digite o nome do centro de custo');
				return;
			}

			$q_sel =
				<<<EOD
SELECT custoID as id FROM custo WHERE custoNome = '$nome_centro'
EOD;
			$res_sel = $conn->executeQuery($q_sel);
			$result = mysql_fetch_object($res_sel)->id;

			if ($result) {
				add_msg(WARNING, 'Centro de custo ja cadastrado');
				return;
			}
			$q =
				<<<EOD
INSERT INTO custo(custoNome) VALUES('$nome_centro');
EOD;
			$res = $conn->executeQuery($q);

			if ($res) {
				add_msg(SUCCESS, 'Centro de custo cadastrado com sucesso');
        return;
			} else {
				add_msg(ERROR, 'Erro ao cadastrar o centro custo');
				return;
			}
		}
	
	}
}

