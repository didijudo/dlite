<?php
class CadastrarFornecedorController extends LayoutController {

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Cadastrar Fornecedor </h1>
				</div>
				<d:form method="post" action="#">
					<d:input id="idNome" name="nome"
						placeholder="Nome do fornecedor" icon="hospital-o"/>
					<d:input id="idCnpj" name="cnpj"
						placeholder="CNPJ do fornecedor" icon="building"/>
				</d:form>
			</x:frag>;
	}
	public function processRequest() {
		Gandalf::check();
		js_call("$('#idNome').maiusculo();");
		js_call("$('#idCnpj').mask('00.000.000/0000-00');");

		$r = $this->getRequest();
		if (is_post()) {
			$nome = $r->getString('nome');
			$cnpj = $r->getString('cnpj');

			if (!$nome) {
				add_msg(ERROR, 'Digite o nome do fornecedor');
				return;
			}
			if (!$cnpj) {
				add_msg(ERROR, 'Digite CNPJ do fornecedor');
				return;
			}
			$conn = dbconn();

			$q_sel =
				<<<EOD
SELECT fornecedorID as id FROM fornecedor WHERE fornecedorCnpj = '$cnpj';
EOD;
			$res_sel = $conn->executeQuery($q_sel);
			$existe = mysql_fetch_object($res_sel)->id;
			if ($existe) {
				add_msg(ERROR, 'CNPJ já cadastrado');
				return;
			}

			$q =
				<<<EOD
INSERT INTO fornecedor(fornecedorNome, fornecedorCnpj) VALUES('$nome', '$cnpj');
EOD;
			$res = $conn->executeQuery($q);
			if ($res) {
				add_msg(SUCCESS, 'Fornecedor cadastrado com sucesso');
				return;
			} else {
				add_msg(ERROR, 'Erro ao cadastrar o fornecedor');
				return;
			}
		}

	}
}
