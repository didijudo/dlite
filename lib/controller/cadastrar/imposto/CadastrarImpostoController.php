<?php
final class CadastrarImpostoController extends LayoutController {
	public $js = array('utils', 'entrada');
	private $select, $impostos;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Cadastrar Imposto </h1>
				</div>
				<d:form method="post" action="#">
					<d:input
						id="idNome" 
						name="nome" 
						placeholder="Nome do imposto"
			 			icon="money"/>
					<d:input id="idValor" 
						name="percent" 
						class="form-control"
						icon="dollar"
						placeholder="%"/>
					{$this->select}
					<d:select id="idConta" name="conta" class="form-control" icon="bank"/>
				</d:form>
				<div class="col-xs-6 col-xs-offset-3">
					<d:accordion head={"Impostos cadastrados"}>
						{$this->impostos}
					</d:accordion>
				</div>
			</x:frag>;
	}


	public function processRequest() {
		Gandalf::check();
		js_call("$('#idNome').maiusculo();");
		js_call("$('#idValor').mask('###.99', {reverse: true});");
		$conn = dbconn();

		$q_impostos =
			<<<EOD
SELECT 
	impostoNome as nome,
	impostoValor as valor
FROM imposto;
EOD;
		$res_imposto = $conn->executeQuery($q_impostos);

		$this->impostos = <ul class="list-unstyled col-xs-12"/>;
		while ($o = mysql_fetch_object($res_imposto)) {
			$link =
				<x:frag>
					<a onclick={"excluir(".$o->id.")"}
						class="btn btn-danger btn-xs prepend-left pull-right" title="Remover">
						<i class="fa fa-trash"/>
					</a>
					<hr/>
				</x:frag>;
			$this->impostos->appendChild(
				<li>
					<h5>
						{$o->nome} | {number_format($o->valor*100, 2, ',', '.')." %"}
						<span>
							{$link}
						</span>
					</h5>
				</li>
			);
		}

		$q =
			<<<EOD
SELECT 
	p.programaNome as nome, 
	p.programaID as id
FROM programa p;
EOD;
		$res = $conn->executeQuery($q);
		$this->select = <d:select id="idPrograma" name="programa" class="form-control" icon="book" 
			placeholder="Selecione o Programa"/>;
		while($o = mysql_fetch_object($res)) {
			$this->select->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);
		}

		if (is_post()) {
			$r = $this->getRequest();
			$conta_id = $r->getInt('conta');
			$nome = $r->getString('nome');
			$percent = moeda(($r->getFloat('percent')/100));

			
			if (!$nome) {
				add_msg(ERROR, 'Digite um nome para o imposto');
				return;
			}
			if (!$conta_id) {
				add_msg(ERROR, 'Escolha uma conta para receber o imposto');
				return;
			}
			if (!$percent) {
				add_msg(ERROR, 'Digite a porcentagem a ser cobrada pelo imposto');
				return;
			}

			$q_select =
				<<<EOD
SELECT impostoNome as nome FROM imposto WHERE impostoNome = '$nome';
EOD;
			$res_select = $conn->executeQuery($q_select);
			$num = mysql_num_rows($res_select);

			if ($num != 0) {
				add_msg(ERROR, 'Imposto já cadastrado');
				return;
			} 
			
			$q_imposto =
				<<<EOD
INSERT INTO imposto(impostoNome, impostoValor, contaID) 
VALUES('$nome', $percent, $conta_id);
EOD;
			$q_custo = 
				<<<EOD
INSERT INTO custo(custoNome, custoTipo) VALUES('$nome', 1);
EOD;
			$conn->beginTransaction();
			$res_custo = $conn->executeQuery($q_custo);
			$res_imposto = $conn->executeQuery($q_imposto);

			if ($res_imposto && $res_custo) {
				$conn->commitTransaction();
				add_msg(SUCCESS, 'Imposto '.$nome.' cadastrado com sucesso');
				return;
			} else {
				$conn->rollbackTransaction();
				add_msg(ERROR, 'Erro ao cadastrar o imposto '.$nome);
				return;
			}
		}
	}
}
