<?php
final class CadastrarProgramaController extends LayoutController {
	private $selectConta, $selectSec;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Cadastrar Programa </h1>
				</div>
				<d:form action="#" method="post" lblButton="Cadastrar">
					<d:input 
						id="idNome"
						type="text" 
						name="nome" 
						icon="book" 
						placeholder="Nome Programa"/>	

					{$this->selectSec}
				</d:form>
			</x:frag>;
	}

	public function processRequest() {
		Gandalf::check();
		js_call("$('#idNome').maiusculo();");

		$conn = dbconn();
		$q1 =
			<<<EOD
SELECT 
	s.secretariaNome as nome,
	s.secretariaID as id
FROM
	secretaria s;
EOD;
		$this->selectSec = 
			<d:select name="secretaria" class="form-control" icon="briefcase" placeholder="Selecione uma secretaria"/>;
		$res1 = $conn->executeQuery($q1);
		while($o = mysql_fetch_object($res1)) {
			$this->selectSec->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);	
		} 

		if (is_post()) {
			$r = $this->getRequest();
			$nome = $r->getString('nome');
			$conta = $r->getInt('conta');
			$secretaria = $r->getInt('secretaria');

			//Guardas

			if (!$nome) {
				add_msg(ERROR, 'Digite um nome para o programa');
				return;
			}

			if (!$secretaria) {
				add_msg(ERROR, 'Selecione uma secretaria para este programa');
				return;
			}

			$q1 =
				<<<EOD
INSERT INTO programa(programaNome, secretariaID) VALUES('$nome', $secretaria);
EOD;
			$res = $conn->executeQuery($q1);

			if ($res) {
				add_msg(SUCCESS, 'Programa cadastrado com sucesso');
				return;
			} else {
				add_msg(ERROR, 'Algo errado aconteceu. =(');
				return;
			}
		}
	}
}
