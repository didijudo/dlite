<?php
final class CadastrarSecretariaController extends LayoutController {

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Cadastro de Secretaria </h1>	
				</div>
				<d:form action="#" method="post" lblButton="Cadastrar">
					<d:input
			 			type="text"
						id="idSecretaria"
						name="secretaria"
						icon="book"
						placeholder="Nome da secretaria"/>
					<d:select name="cor" icon="th-large"
						class="form-control" placeholder="Escolha a cor">
						<option value="sky" 
							style="background: #009688">Céu
						</option>
						<option value="orange"
							style="background: #ff9800">Laranja
						</option>
						<option value="brown"
							style="background: #694a3e">Marrom
						</option>
						<option value="midnightblue"
							style="background: #37474f">Cinza
						</option>
						<option value="success"
							style="background: #7eb73d">Verde
						</option>
						<option value="primary"
							style="background: #0398db">Azul
						</option>
						<option value="danger"
							style="background: #d0181e">Vermelho
						</option>
						<option value="magenta"
							style="background: #d81557">Rosa
						</option>
					</d:select>
				</d:form>
			</x:frag>;

	}

	public function processRequest() {
		Gandalf::onlyAdmin();
		js_call("$('#idSecretaria').maiusculo()");

		if (is_post()) {
			$r = $this->getRequest();
			$nome = $r->getString('secretaria');
			$cor = $r->getString('cor');

			if (!$nome) {
				add_msg(ERROR, 'Digite o nome da secretaria');
				return;
			}
			if (!$cor || $cor == '#FFFFFF') {
				add_msg(ERROR, 'Escolha uma cor');
				return;
			}

			$conn = dbconn();
			$q =
				<<<EOD
INSERT INTO secretaria(secretariaNome, secretariaCor) VALUES('$nome', '$cor');
EOD;
			$res = $conn->executeQuery($q);

			if ($res) {
				add_msg(SUCCESS, 'Secretaria '.$nome.' criada com sucesso');
				return;
			} else {
				add_msg(ERROR, 'Algo errado aconteceu =(');
				return;
			}
		}
	}
}
