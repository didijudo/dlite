<?php
final class CadastrarUsuarioController extends LayoutController {
	private $select;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Cadastro de Usuário </h1>
				</div>
				<div class="row">
					<div class="col-xs-6 col-xs-offset-3">
						<div class="panel panel-default">
							<div class="panel-body">
								<form action="#" method="post" class="form-horizontal">
									<d:input 
										type="email" 
										name="email" 
										placeholder="Email" icon="envelope"/>

									<d:input 
										id="idNome"		
										type="text"
										name="nome"
									 	placeholder="Nome Completo" icon="user"/>

									<div class="form-group">
										<div class="col-xs-12">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-user"></i>
												</span>
												{$this->select}
											</div>
										</div>
									</div>

									<div class="panel-footer">
										<div class="clearfix">
											<input type="submit" 
												class="btn btn-primary pull-right" 
												value="Cadastrar"/>
										</div>
									</div>
								</form>	
							</div>
						</div>
					</div>
				</div>
			</x:frag>;
	}

	public function processRequest() {
		Gandalf::onlyAdmin();
		js_call("$('#idNome').maiusculo()");
		$conn = dbconn();
		$q0 =
			<<<EOD
SELECT 
	perfilID as id,
	perfilNome as nome
FROM perfil
EOD;
		$this->select = <select name="perfil" class="form-control"/>;
		$result = $conn->executeQuery($q0);
		while($o = mysql_fetch_object($result)) {
			$this->select->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);
		}


		if (is_post()) {
			$r = $this->getRequest();
			$email = $r->getString('email');
			$nome = $r->getString('nome');
			$senha = hash_string(5);
			$perfil = $r->getInt('perfil', 1);

			if (!$email) {
				add_msg(ERROR, 'Digite o endereço de email.');
				return;
			}

			if (!$nome) {
				add_msg(ERROR, 'Digite o nome.');
				return;
			}

			/*if (!$senha) {
				add_msg(ERROR, 'Digite a senha');
				return;
			}*/

			if (!$perfil) {
				add_msg(ERROR, 'Escolha um perfil');
				return;
			}

			if(existe_email($email, $conn)) {
				add_msg(ERROR, 'Email já cadastrado!');
				return;
			}

			//TODO Fazer o bloqueio do usuario functionar
			$senhaCripto = md5($senha);
			$q =
				<<<EOD
insert into usuarios(usuarioEmail, usuarioSenha, usuarioNome, usuarioBloqueio, usuarioNivel, usuarioAcesso) values('$email', '$senhaCripto', '$nome', 0, $perfil, 1);
EOD;
			$res = $conn->executeQuery($q);
			if (!$res) {
				add_msg(ERROR, 'Algo errado aconteceu! -> '.mysql_error());
				return;
			} else {
				$q00 =
					<<<EOD
SELECT perfilNome as nome FROM perfil WHERE perfilID = $perfil
EOD;
				$res2 = $conn->executeQuery($q00);

				$perfil_name = mysql_fetch_object($res2)->nome;

				/*
				 * 	A variavel $body abaixo é responsável pelo corpo da mensagem
				 * que será enviada por email.
				 */

				$body =
					<<<EOD
						<div>
							Olá {$nome},<br/>
							Você foi adicionado ao sistema de 
							finanças com o perfil de <b>{$perfil_name}</b>. <br/>
							Utilize login: {$email}
							Sua senha para o primeiro acesso é <a href=""><b>{$senha}</b></a><br/>
							Acesse <a href="www.sefincaninde.com.br" target="_blank"> Financas </a> 
							Lembre de alterar sua senha no primeiro acesso!
							<footer> 
								<h4 style="margin: 0;"> &copy; 2015 Somar Gestão pública</h4>
							</footer>
						</div>
EOD;

				/**
				 * A variavel $to é o destinatário do email.
				 * Deve ser construido da seguinte maneira
				 * 				'nome:email'
				 * 				'Diego Andrade:didi.ufs@gmail.com'
				 * 				'Marcelo Wazaa:marcelowazaa$gmail.com'
				 *
				 * pode ser construido para colocar mais de um email
				 * Ex. 'Diego:didi.ufs@gmail.com,Marcelo:marcelowazaa@gmail.com'
				 */
				$to = "$nome:$email";

				/**
				 * A função send_mail, envia um email e tem como parametros
				 * os seguintes campos
				 *	1) Assunto
				 *	2) Corpo da mensagem
				 *	3) destinatario
				 */
				$sent = send_mail('Ativação da conta', $body, $to);
				if ($sent) {
					add_msg(SUCCESS, 
						'Usuario cadastrado com sucesso. Um email foi enviado para o usuário');
				}
				send_redirect('/listar/usuarios');
				return;
			}
		}
	}
}
