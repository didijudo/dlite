<?php
final class ContasBancoController extends LayoutController {
	private $select;
	private $contas;
	private $tabela;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Contas por banco </h1>
				</div>	
				<div class="row">
					<div class="col-xs-12">
						<d:form
							lblButton="Buscar"
							title="Banco"
							action="#"
							method="post">
							<d:select 
								icon="bank" 
								name="banco"
								placeholder="Selecione o banco"
								class="form-control">
								{$this->select}
							</d:select>
						</d:form>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="panel panel-sky prepend-top">
							<div class="table-responsive">
								{$this->tabela}	
							</div>
						</div>
					</div>
				</div>
			</x:frag>;
	}
	
	public function processRequest() {
		Gandalf::check();
		$conn = dbconn();
		$q_banco =
			<<<EOD
SELECT 
	bancoNome as nome, 
	bancoID as id
FROM banco;
EOD;
		$res_banco = $conn->executeQuery($q_banco);
		$this->select = <x:frag/>;
		while($o = mysql_fetch_object($res_banco)) {
			$this->select->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);
		}

		if (is_post()) {
			$r = $this->getRequest();
			$id_banco = $r->getInt('banco');
			$q_contas =
				<<<EOD
SELECT 
	contaID as id, 
	contaNome as nome,
	contaNumero as numero,
	contaAgencia as agencia,
	contaSaldo as saldo
FROM contas
WHERE bancoID = $id_banco;
EOD;
			$res_contas = $conn->executeQuery($q_contas);
			$this->tabela = 
				<table class="table table-hover">
					<tr> 
						<th>#AGENCIA</th> 
						<th>#NÚMERO</th>
						<th>#SALDO</th>
						<th>#NOME</th>
						<th>#AÇÃO</th>
					</tr>
				</table>;
			while($o = mysql_fetch_object($res_contas)) {
				$this->tabela->appendChild(
					<tr>
						<td> <b>{$o->agencia}</b> </td>
						<td> <b>{$o->numero}</b> </td>
						<td> <b>{'R$ '.$o->saldo}</b> </td>
						<td> <b>{$o->nome}</b> </td>
						<td>
							<a
								title="Visualizar Conta"
					 			href={"/conta/detalhes?id=".$o->id} 
								class="btn btn-default">
								<i class="fa fa-eye"/>
							</a> 
						</td>
					</tr>
				);	
			}
		}
	}
}
