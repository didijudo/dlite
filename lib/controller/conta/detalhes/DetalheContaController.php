<?php
final class DetalheContaController extends LayoutController {
	public $js = array('jquery.tokenize');
	public $css = array('jquery.tokenize');

	private $tabela;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Detalhes da conta </h1>
				</div>
				<div class="col-xs-12">
					<div class="row">
						<div class="panel panel-sky">
							<div class="panel-heading">
								Movimentações da conta
							</div>
							<div class="row">
								<div class="panel-body">
									{$this->tabela}
								</div>
							</div>
						</div>	
					</div>
				</div>
				<div class="row">
					<div id="chart2"/>
				</div>
			</x:frag>;
	}
	public function processRequest() {
		Gandalf::check();
		js_call("
			var colunas = [
				{'sTitle': 'Data'},
				{'sTitle': 'Tipo'},
				{'sTitle': 'Valor'},
				{'sTitle': 'Centro de custo'},
				{'sTitle': 'Usuario Cadastrante'},
			];
			var el = $('#idTabela');
		 tabelaDinamica(el, colunas);	
		");
		$r = $this->getRequest();
		$conta_id = $r->getInt('id');

		$q =
			<<<EOD
SELECT 
	e.extratoDataMovimentacao as data,
	t.tipoNome as tipo,
	e.extratoTipo as mov,
	e.extratoValor as valor,
	u.usuarioNome as nome,
	c.custoNome as custo
FROM extrato e, usuarios u, tipoMovimentacao t, custo c
WHERE e.usuarioID = u.usuarioID
	AND e.extratoTipo = t.tipoID
	AND e.custoID = c.custoID
	AND e.contaID = $conta_id
ORDER BY e.extratoDataMovimentacao DESC;
EOD;
			$this->tabela = 
				<table class="table table-striped table-bordered table-hover" 
					id="idTabela"/>;
		$conn = dbconn();
		$res = $conn->executeQuery($q);
		if ($res) {
			while($o = mysql_fetch_object($res)) {
				$o->custo = mb_convert_encoding($o->custo, 'UTF-8', 'LATIN1');
				$class = ($o->mov == 1) ? "success" : "danger";
				$this->tabela->appendChild(
					<tr class={$class}>
						<td> {date("d/m/Y", strtotime($o->data))} </td>
						<td> {$o->tipo} </td>
						<td> R$ {money_format('%!i', $o->valor)} </td>
						<td> { $o->custo } </td>
						<td> {$o->nome} </td>
					</tr>
				);
			}
		}
	}
}
