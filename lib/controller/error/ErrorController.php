<?php
final class ErrorController extends LayoutController {
  public $js = array('utils');

  public function setContent() {
    header('Status: 404', false, 404);
    return
      <x:frag>
				<div class="page-heading">
					<div class="row">
						<div class="col-md-12">
							<h1 class="">404 - Página não encontrada! =( </h1>
						</div>
					</div>
				</div>
				<div class="container">
					<p> Essa página ainda não foi criada! </p>
					<h4> <a href="/home">Home </a></h4>
				</div>
      </x:frag>;
  }

	public function composeMenu() {}
	public function composeMenuTop() {}

  public function processRequest() {

  }

}
