<?php
final class ExcluirCustoController extends RestController {

	private $array;

	public function restResponse() {
		return $this->array;
	}
	
	public function processRequest() {
		Gandalf::needLogin();
		$r = $this->getRequest();
		if (is_post()) {
			$id = $r->getInt('id');

			$q =
				<<<EOD
SELECT extratoID as id 
FROM extrato
WHERE custoID = $id 
EOD;
			$conn = dbconn();
			$res = $conn->executeQuery($q);
			$ids = mysql_fetch_object($res)->id;

			if (!$ids) {
				$q_del =
					<<<EOD
DELETE FROM custo WHERE custoID = $id;
EOD;

				$res_del = $conn->executeQuery($q_del);
				if ($res_del) {
					$this->array[] = array('result' => 'ok');
				} else {
					$this->array[] = array('result' => 'error');
				}
			} else {
				$this->array[] = 
					array(
						'result' => 'error', 
						'msg' => 'Este custo já foi utilizado.');
			} 
		}
	}
}
