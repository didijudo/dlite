<?php
final class ExcluirPerfilController extends RestController {

	private $array;

	public function restResponse() {
		return $this->array;
	}
	
	public function processRequest() {
		Gandalf::needLogin();
		$r = $this->getRequest();
		if (is_post()) {
			$id = $r->getInt('id');

			$q =
				<<<EOD
SELECT usuarioNome as nome 
FROM usuarios 
WHERE usuarioNivel = $id 
	AND usuarioBloqueio <> 2
EOD;
			$conn = dbconn();
			$res = $conn->executeQuery($q);
			$nome = mysql_fetch_object($res)->nome;

			if (!$nome) {
				$q_del =
					<<<EOD
DELETE FROM perfil WHERE perfilID = $id;
EOD;

				$res_del = $conn->executeQuery($q_del);
				if ($res_del) {
					$this->array[] = array('result' => 'ok');
				} else {
					$this->array[] = array('result' => 'error');
				}
			} else {
				$this->array[] = 
					array(
						'result' => 'error', 
						'msg' => 'Existe usuário utilizando o perfil.');
			} 
		}
	}
}
