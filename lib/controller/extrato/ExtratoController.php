<?php
final class ExtratoController extends LayoutController {
	public $js = array('jquery.tokenize', 'exportar');
	public $css = array('jquery.tokenize');
	private $contas, $conta, $tabela, $selectCusto, $totalEntrada, $totalSaida;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Extrato </h1>
				</div>
				<div class="col-xs-12">
					<d:form 
						lblButton="Buscar"
						tamanho="col-xs-8 col-xs-offset-2"
						title="Extrato"
						action="#" method="post">
						<div class="col-xs-12">
							<d:select 
								placeholder="Tipo de movimentação" name="movimentacao"
								class="form-control" icon="exchange">
									<option value="1"> Entrada </option>
									<option value="2"> Saída </option>
							</d:select>
						</div>
						<div class="col-xs-6">
							<d:input type="text" icon="calendar" id="idInicio"
					 			name="inicio" placeholder="Data Inicio"/>
						</div>
						<div class="col-xs-6">
							<d:input type="text" icon="calendar" id="idFim"
					 			name="fim" placeholder="Data Fim"/>
						</div>
						{$this->selectCusto}
					</d:form>
				</div>
				<hr/>
				<div class="col-xs-12">
					<div class="row">
						<div class="panel panel-default">
							<div class="panel-heading">
								Resultado
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<label>TOTAIS </label> 
									</div>
									<div class="col-md-12">
										<b>
											Entrada: R$ {number_format($this->totalEntrada, 2, ',', '.')} <br/>
											Saida: R$ {number_format($this->totalSaida, 2, ',', '.')}
										</b>
									</div>
								</div>
								{$this->tabela}
							</div>
						</div>	
					</div>
				</div>
			</x:frag>;
	}
	public function processRequest() {
		Gandalf::onlyAdmin();
		js_call("$('#idCusto').tokenize({'placeholder': 'Centro de custos'});");
		js_call("$('#idFim').datepicker({'language': 'pt-BR', 'autoclose': 'true'});");
		js_call("$('#idInicio').datepicker({'language': 'pt-BR', 'autoclose': 'true'});");

		$conn = dbconn();
		$q_custo =
			<<<EOD
SELECT 
	custoNome as nome,
	custoID as id
FROM custo;
EOD;
		$this->selectCusto = 
			<select name="custo[]"
				id="idCusto" multiple="multiple" class="col-xs-12" style="height: 20px"/>;
		$res_custo = $conn->executeQuery($q_custo);
		while($o = mysql_fetch_object($res_custo)) {
			$o->nome = mb_convert_encoding($o->nome, 'UTF-8', 'LATIN1');
			$this->selectCusto->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);
		}	

		if (is_post()) {
			$r = $this->getRequest();
			$data_ini = $r->getString('inicio');
			$data_fim = $r->getString('fim');
			$tipo = $r->getInt('movimentacao');
			$custos = $r->getArray('custo');

			$and_custo = "";
			$tabela_custo = "";
			$custo_label = "";
			if ($custos) {
				$tabela_custo = ", custo c";
				$custo_label = ", c.custoNome as custo";

				$in = "";
				for ($i =0; $i < count($custos); $i++) {
					if ($i == 0) {
						$in = $custos[$i];
						continue;
					}
						$in .= ", ".$custos[$i];
				}
				$and_custo =
					<<<EOD
AND e.custoID in ($in)
EOD;
			}

			if ($data_ini) {
				if (!$data_fim) {
					$data_fim = date('d/m/Y');
				}
				$and =
					<<<EOD
	AND e.extratoDataMovimentacao BETWEEN STR_TO_DATE('$data_ini', '%d/%m/%Y') AND STR_TO_DATE('$data_fim', '%d/%m/%Y') 
EOD;
			}

			$and_tipo;
			if ($tipo) {
				$and_tipo =
					<<<EOD
AND e.extratoTipo = $tipo
EOD;
				if ($tipo == 2) {
					$custo_label = ", c.custoNome as custo";
				}
			}

			$q_saida =
				<<<EOD
SELECT 
	e.extratoDataMovimentacao as data,
	t.tipoNome as tipo,
	e.extratoTipo as mov,
	e.extratoValor as valor,
	u.usuarioNome as nome,
	c.custoNome as custo
FROM extrato e, usuarios u, tipoMovimentacao t, custo c
WHERE e.usuarioID = u.usuarioID
	AND e.extratoTipo = t.tipoID
	AND e.custoID = c.custoID
	$and_tipo
	$and_custo
	$and
EOD;
			$res_saida = $conn->executeQuery($q_saida);
			$this->tabela = 
				<table 
					class="table table-striped table-bordered table-hover" id="idTabela"/>;
			if (!$res_saida) {
				add_msg(ERROR, 'Algo errado aconteceu! Estamos trabalhando...');
				return;
			}
			while($o = mysql_fetch_object($res_saida)) {
				//$o->tipo = mb_convert_encoding($o->tipo, 'UTF-8', 'LATIN1');
				$o->custo = mb_convert_encoding($o->custo, 'UTF-8', 'LATIN1');
				$class = ($o->mov == 1) ? "success" : "danger";

				if ($o->mov == 1) {
					$this->totalEntrada += $o->valor;
				} else {
					$this->totalSaida += $o->valor;
				}

				$this->tabela->appendChild(
					<tr class={$class}>
						<td> {date("d/m/Y", strtotime($o->data))} </td>
						<td> {$o->tipo} </td>
						<td> R$ {money_format('%!i', $o->valor)} </td>
						<td> { $o->custo } </td>
						<td> {$o->nome} </td>
					</tr>
				);
			}
		}
	}
}
