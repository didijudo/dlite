<?php
final class GestorController extends LayoutController {
	private $select;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Escolher gestor </h1>
				</div>
				<d:painel titulo="Alterar Gestor" 
					botao="Entendi" idBtn="idBotao" close="false">
					<p>
						O Gestor receberá emails enviados 
							do sistema para ajudar na tomada de decisões.<br/>
							É de suma importância que tenha entendido a 
							importância do gestor!!<br/>
							Se você concorda concorda em alterar o gestor,
							clique no "Entendi" e altere o gestor. 
					</p>
				</d:painel>
				<d:form 
					action="#" 
					method="post"
					title="Escolher Gestor"
					lblButton="Escolher"
				 	classBtn="disabled"
					idBtn="idBtn">
					{$this->select}	
				</d:form>
			</x:frag>;
	}

	public function processRequest() {
		Gandalf::onlyAdmin();
		js_call(
			"$('#idBotao').click(function() {
				$('#idBtn').removeClass('disabled');
			})"
		);

		$q =
			<<<EOD
SELECT
	u.usuarioID as id, 
	u.usuarioNome as nome,
	u.gestor as gestor
FROM usuarios u;
EOD;

		$conn = dbconn();
		$res = $conn->executeQuery($q);

		$this->select = <d:select name="gestor" class="form-control" icon="gears"/>;
		while($o = mysql_fetch_object($res)) {
			$selected = 'false';
			if ($o->gestor == 1) {
				$selected = 'true';
			}
			$this->select->appendChild(
				<option value={$o->id} selected={$selected}> {$o->nome} </option>
			);
		}

		if (is_post()) {
			$r = $this->getRequest();
			$id = $r->getInt('gestor');

			if (!$id) {
				add_msg(ERROR, 'Escolha um gestor');
				return;
			}

			$q1 =
				<<<EOD
UPDATE usuarios SET gestor=0;
EOD;
			$q2 =
				<<<EOD
UPDATE usuarios SET gestor=1 WHERE usuarioID=$id;
EOD;
			$res1 = $conn->executeQuery($q1);
			$res2 = $conn->executeQuery($q2);
			if ($res2) {
				add_msg(WARNING, 'Gestor alterado com sucesso');
				send_redirect('/listar/usuarios');
			} else {
				add_msg(ERROR, 'Algo errado aconteceu');
				return;
			}
		} 
	}
}
