<?php
class HomeController extends LayoutController {
	public $js = array('home');
	private $total;
	private $linha;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1>Balanço das contas </h1>
				</div>
				<div class="row">
					<div class="col-md-7">
						{$this->linha}
					</div>
					<div class="col-md-5">
						<div id="grafico" style="width:100%; height: 350px" class="centered">
						</div>
						<d:tile tamanho="8" classe="col-md-offset-1 prepend-top"
							centro={money_format('%!i', $this->total)}
							titulo="Saldo total"/>
					</div>
				</div>
			</x:frag>;
	}

	public function processRequest() {
		if (!Gandalf::isLogged()) {
			add_msg(ERROR, 'Você precisa esta logado');
			send_redirect('/logout');
		}
		$q =
			<<<EOD
SELECT 
	secretariaNome as nome,
	secretariaID as id,
	secretariaCor as cor
FROM 
	secretaria
EOD;

		$conn = dbconn();
		$this->linha = <div class="col-xs-12"/>;
		$res = $conn->executeQuery($q);
		
		while($o = mysql_fetch_object($res)) {
			$q_sum =
				<<<EOD
SELECT 
	SUM(contaSaldo) AS saldo 
FROM contas 
WHERE programaID IN 
	(SELECT programaID FROM programa WHERE secretariaID = $o->id )
EOD;
			$res_sum = $conn->executeQuery($q_sum);
			$saldo = mysql_fetch_object($res_sum)->saldo;
			$this->total += $saldo;
			$this->linha->appendChild(
				<d:colorbox 
					tamanho="6"
					central={"R$ ".money_format('%!i', $saldo)}
					titulo={$o->nome} 
					cor={$o->cor}
					link={"/secretaria/detalhes?id=".$o->id}/>
			);	
		}
	}
}
