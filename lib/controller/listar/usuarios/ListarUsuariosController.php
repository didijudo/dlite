<?php
final class ListarUsuariosController extends LayoutController {
	public $js = array('listarUsuario');
	private $tabela;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Listagem de usuários </h1>
				</div>
				<div class="page-body">
					<div>
						<a href="/cadastrar/usuario" class="btn btn-success">
							<i class="fa fa-user"/>
							Novo Usuário
						</a>
						<a href="/perfil/criar" class="btn btn-primary prepend-left">
							<i class="fa fa-gear"/>
							Criar Perfil
						</a>
						<a href="/gestor" class="btn btn-info pull-right">
							<i class="fa fa-eye"/>
							Alterar Gestor
						</a>
					</div>
					<div class="panel panel-sky prepend-top">
						<div class="table-responsive">
							{$this->tabela}	
						</div>
					</div>
				</div>
			</x:frag>;
	}

	public function processRequest() {
		Gandalf::check();

		$q =
			<<<EOD
SELECT 
	u.usuarioNome as nome,
	u.usuarioID as id,
	p.perfilNome as perfil
FROM usuarios u, perfil p
WHERE u.usuarioNivel = p.perfilID
	AND u.usuarioBloqueio <> 2
EOD;


		$conn = dbconn();
		$res = $conn->executeQuery($q);

		$this->tabela = 
			<table class="table table-hover">
				<tr> 
					<th>NOME</th> 
					<th>PERFIL</th>
					<th>EDITAR</th>
					<th>EXCLUIR</th>
				</tr>
			</table>;
		while($o = mysql_fetch_object($res)) {
			$cor = ($o->perfil == 'ADMINISTRADOR') ? 'success': 'primary';
			$this->tabela->appendChild(
				<tr>
					<td> <b>{$o->nome}</b> </td>
					<td> <span class={"label label-".$cor}>{$o->perfil} </span> </td>
					<td> 
						<a href={"/alterar/usuario/admin?id=".$o->id} class="btn btn-primary">
							<i class="fa fa-edit"/>
						</a> 
					</td>
					<td> <a href={"javascript:excluir(".$o->id.")"}  class="btn btn-danger">
						<i class="fa fa-close"/></a> 
					</td>
				</tr>
			);
		}
	}
}
