<?php
final class LoginController extends LayoutController {

	public function setContent() {
		return
			<x:frag>
				<div class="row prepend-top">
					<div class="col-xs-3 col-xs-offset-5">
						<img src="/htdocs/img/login-logo.png" height="150" style="margin-bottom: 30px"/>
					</div>
				</div>
				<div class="row prepend-top">
					<div class="col-md-4 col-md-offset-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2>Login</h2>
							</div>
								<div class="panel-body">
									<form action="login" 
										class="form-horizontal" 
										id="validate-form" method="post">

										<div class="form-group">
											<div class="col-xs-12">
												<div class="input-group">							
													<span class="input-group-addon">
														<i class="fa fa-user"></i>
													</span>
													<input type="email" 
														name="email"
														class="form-control"
														placeholder="Email Username"
														data-parsley-minlength="6" 
														placeholder="Email" required=""/>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-xs-12">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="fa fa-key"></i>
													</span>
													<input type="password" 
														name="senha"
														class="form-control" 
														id="exampleInputPassword1"
														placeholder="Senha"/>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-xs-12">
												<a href="/alterar/usuario/recuperar" 
													class="pull-left">Esqueceu a senha?
												</a>
										</div>
									</div>
								
									<div class="panel-footer">
										<div class="clearfix">
											<input type="submit" 
												class="btn btn-primary pull-right" 
												value="Entrar"/>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</x:frag>;
	}

	public function composeMenuTop() {}
	public function composeMenu() {}

	public function processRequest() {
		if (Gandalf::isLogged()) {
			send_redirect('/home');
		}

		if (is_post()) {
			$r = $this->getRequest();
			$email = $r->getString('email');
			$senha = $r->getString('senha');
			$senha = md5($senha);

			if (!$email) {
				add_msg(ERROR, 'Digite um email!');
				return;
			}
			$q =
				<<<EOD
SELECT 
	usuarioID as id, 
	usuarioNome as nome, 
	usuarioEmail as email,
	usuarioBloqueio as bloqueado,
	usuarioNivel as nivel,
	usuarioAcesso as acesso
FROM usuarios 
WHERE usuarioEmail = '$email' 
	AND usuarioSenha = '$senha'
	AND usuarioBloqueio <> 2
EOD;
			$conn = dbconn();
			$res = $conn->executeQuery($q);

			if (mysql_num_rows($res) == 1) {
				$o = mysql_fetch_object($res);
				if ($o->bloqueado == 1) {
					add_msg(ERROR, "Usuario ainda nao desbloquado");
					return;
				} else {
					$_SESSION['usuario'] = $o;
					if ($o->acesso == 1) {
						$q_alter =
							<<<EOD
UPDATE usuarios SET usuarioAcesso = 0 WHERE usuarioID = $o->id;
EOD;
						$res_alter = $conn->executeQuery($q_alter);
						add_msg(WARNING, 'Altere a senha para sua segurança');
						send_redirect('/alterar/usuario/senha');
					} else {
						$_SESSION['usuario'] = $o;
						send_redirect('/home');
					}
				}
			} else {
					add_msg(ERROR, "Login e/ou senha inválidos");
					return;
			}
		}
	}
}
