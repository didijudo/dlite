<?php
final class LogoutController extends Controller {
	
	public function processRequest() {
		//TODO logout do sistema
		unset($_SESSION['usuario']);
		send_redirect('/login');
	}
}
