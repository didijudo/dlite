<?php
final class EntradaController extends LayoutController {
	public $js = array('utils', 'entrada');
	private $select;
	
	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Registrar Entrada </h1>
				</div>
				<d:form title="Entrada" method="post">
					{$this->select}	
					<d:select id="idConta" name="conta" class="form-control" icon="bank"/>
					<d:input
						type="text"
						name="descricao"
						icon="file"
						id="idDescricao"
						placeholder="Descrição"/>
					<d:input
				 		type="text"
						name="valor"
						icon="money"
						id="idValor"
						placeholder="Valor"/>
					<d:input
						type="text"
						name="data"
						class="data"
						icon="calendar"
						id="idData"
						placeholder="Data da entrada"/>
				</d:form>
			</x:frag>;
	}

	public function processRequest() {
		Gandalf::check();
		$r = $this->getRequest();

		js_call("$('#idValor').mask('000.000.000.000,00', {reverse: true})");
		js_call("$('#idData').datepicker({'language': 'pt-BR', 'autoclose': 'true'})");

		$q =
			<<<EOD
SELECT 
	p.programaNome as nome, 
	p.programaID as id
FROM programa p;
EOD;
		$conn = dbconn();
		$res = $conn->executeQuery($q);
		$this->select = <d:select id="idPrograma" name="programa" class="form-control" icon="book" 
			placeholder="Selecione o Programa"/>;
		while($o = mysql_fetch_object($res)) {
			$this->select->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);
		}

		if (is_post()) {
			$conta_id = $r->getInt('conta');
			$programa_id = $r->getInt('programa');
			$valor = moeda($r->getString('valor'));
			$descricao = $r->getString('descricao');
			$data_entrada = $r->getString('data'); 
			$controle = get_controle();
			$usuario_id = Gandalf::getUsuario()->id;

			if (!$programa_id) {
				add_msg(ERROR, "Um programa precisa ser selecionado");
				return;
			}

			if (!$conta_id) {
				add_msg(ERROR, "Uma conta precisa ser selecionada");
				return;
			}

			if (!$valor) {
				add_msg(ERROR, "Um valor precisa ser informado");
				return;
			}

			if (!$data_entrada) {
				add_msg(ERROR, "Uma data precisa ser informada");
				return;
			}
			if (!$descricao) {
				add_msg(ERROR, "Uma descrição precisa ser informada");
				return;
			}

			$q_insert =
				<<<EOD
INSERT INTO 
extrato(extratoDataMovimentacao, extratoControle, extratoTipo, extratoValor, extratoDataTransacao, contaID, usuarioID, custoID, extratoDescricao) 
VALUES(STR_TO_DATE('$data_entrada', '%d/%m/%Y'), '$controle', 1, $valor, CURRENT_DATE, $conta_id, $usuario_id, 999999, '$descricao');
EOD;
			/**
			 * Inicia uma transacao de banco de dados
			 */
			$conn->beginTransaction();

			$result = $conn->executeQuery($q_insert);

			$q_valor_anterior =
				<<<EOD
SELECT contaSaldo as saldo FROM contas WHERE contaID = $conta_id;
EOD;
			$res_saldo = $conn->executeQuery($q_valor_anterior);
			$saldo_anterior = mysql_fetch_object($res_saldo)->saldo;
			$saldo_atual = $saldo_anterior + $valor;
			$saldo_atual = moeda($saldo_atual);

			$q_update =
				<<<EOD
UPDATE contas SET contaSaldo = $saldo_atual WHERE contaID = $conta_id;
EOD;

			$res_update = $conn->executeQuery($q_update);

			if ($result && $res_saldo && $res_update) {
				$conn->commitTransaction();
				add_msg(SUCCESS, "Entrada $controle cadastrada com sucesso");
				return;
			} else {
				$conn->rollbackTransaction();
				add_msg(ERROR, 
					"Não foi possivel registrar essa entrada. Algo errado aconteceu");
				return;
			}
		}
	}
}
