<?php
final class SaidaController extends LayoutController {
	public $js = array('utils', 'entrada', 'saida', 'jquery.tokenize');
	public $css =
		array('jquery.tokenize', 'dataTables.bootstrap');
	private $select;
	private $custo;
	private $selectImposto;


	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Registrar Saída </h1>
				</div>
				<d:form title="Pagamento" method="post">
					{$this->select}
					<d:select id="idConta" name="conta" class="form-control" icon="bank"/>
					<d:input id="idCusto"
						class="" icon="tag" placeholder="Centro de custo"/>

					<input type="hidden" name="custo" id="idCustoValue"/>
					<d:input
				 		type="text"
						icon="male"
						id="idFornecedor"
						tam="11"
						placeholder="Fornecedor">
							<span id="idNovoFornecedor"
								class="btn btn-primary" title="Novo fornecedor">+</span>
						</d:input>
						<input type="hidden" name="fornecedor" id="idFornecedorValue"/>
						<d:input type="text" id="idDescricao"
							name="descricao" icon="font" placeholder="Descrição"/>

					<d:input
				 		type="text"
						name="valor"
						icon="money"
						id="idValor"
						placeholder="Valor"/>

					{$this->selectImposto}
					<d:input
						type="text"
						name="data"
						class="data"
						icon="calendar"
						id="idData"
						placeholder="Data do pagamento"/>
				</d:form>

				<div id="dialog" class="">
					<form>
						<d:input id="idNomeFor" name="nome"
							placeholder="Nome do fornecedor" icon="hospital-o"/>
						<d:input id="idCnpj" name="cnpj"
							placeholder="CNPJ do fornecedor" icon="building"/>
					</form>
				</div>
			</x:frag>;
	}

	public function processRequest() {
		Gandalf::check();
		js_call("$('#idImposto').tokenize({'placeholder': 'Impostos'});");
		js_call("$('#idValor').mask('000.000.000.000,00', {reverse: true})");
		js_call("$('#idData').datepicker({'language': 'pt-BR', 'autoclose': 'true'})");
		js_call("$('#idCusto').maiusculo();");
		js_call("$('#idNomeFor').maiusculo();");
		js_call("$('#idDescricao').maiusculo();");
		js_call("$('#idFornecedor').maiusculo();");
		js_call("$('#idCnpj').mask('99.999.999/9999-99');");
		
		$conn = dbconn();
		$q_custo =
			<<<EOD
SELECT 
	impostoNome as nome,
	impostoID as id
FROM imposto;
EOD;
		$this->selectImposto = 
			<select name="imposto[]" id="idImposto" class="col-xs-12" style="height: 20px"/>;
		$res_custo = $conn->executeQuery($q_custo);
		while($o = mysql_fetch_object($res_custo)) {
			$o->nome = mb_convert_encoding($o->nome, 'UTF-8', 'LATIN1');
			$this->selectImposto->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);
		}	

		$q_programa =
			<<<EOD
SELECT 
	p.programaNome as nome, 
	p.programaID as id
FROM programa p;
EOD;
		$res = $conn->executeQuery($q_programa);
		$this->select = 
			<d:select id="idPrograma" name="programa" class="form-control" icon="book" placeholder="Selecione o programa"/>;
		while($o = mysql_fetch_object($res)) {
			$this->select->appendChild(
				<option value={$o->id}> {$o->nome} </option>
			);
		}

		if (is_post()) {
			$r = $this->getRequest();
			$conta_id = $r->getInt('conta');
			$custo_id = $r->getInt('custo'); 
			$destinatario = $r->getInt('fornecedor'); 
			$programa_id = $r->getInt('programa');
			$valor = $r->getString('valor');
			$data_saida = $r->getString('data'); 
			$descricao = $r->getString('descricao');
			$controle = get_controle();
			$usuario_id = Gandalf::getUsuario()->id;
			$impostos = $r->getArray('imposto');
			$custo_entrada = array();

			if (!$programa_id) {
				add_msg(ERROR, "Um programa precisa ser selecionado");
				return;
			}

			if (!$conta_id) {
				add_msg(ERROR, "Uma conta precisa ser selecionada");
				return;
			}

			if (!$custo_id) {
				add_msg(ERROR, "Um custo precisa ser selecionado");
				return;
			}

			if (!$destinatario) {
				add_msg(ERROR, "Digite o nome de um destinatario");
				return;
			}

			if (!$valor) {
				add_msg(ERROR, "Um valor precisa ser informado");
				return;
			}

			if (!$data_saida) {
				add_msg(ERROR, "Uma data precisa ser informada");
				return;
			}

			$conn->beginTransaction();
			if (count($impostos) > 0) {
				$desconto = $this->calculaImposto($impostos, $valor, $data_saida);
				$valor = moeda($valor) - $desconto;
			} else {
				$desconto = 0.0;
				$valor = moeda($valor);
			}
			$q_insert =
				<<<EOD
INSERT INTO 
extrato(custoID, extratoDataMovimentacao, extratoControle, extratoTipo, extratoValor, extratoDataTransacao, contaID, usuarioID, extratoDestinatario, extratoDescricao) 
VALUES($custo_id ,STR_TO_DATE('$data_saida', '%d/%m/%Y'), '$controle', 2, $valor, CURRENT_DATE, $conta_id, $usuario_id, $destinatario, '$descricao')
EOD;
			$result = $conn->executeQuery($q_insert);

			$q_valor_anterior =
				<<<EOD
SELECT contaSaldo as saldo FROM contas WHERE contaID = $conta_id;
EOD;
			$res_saldo = $conn->executeQuery($q_valor_anterior);
			$saldo_anterior = mysql_fetch_object($res_saldo)->saldo;

			if ($saldo_anterior < $valor) {
				$conn->rollbackTransaction();
				add_msg(ERROR, "Saldo insuficiente para o pagamento");
				return;
			}
			$saldo_atual = $saldo_anterior - $valor;
			$saldo_atual = moeda($saldo_atual);

			$q_update =
				<<<EOD
UPDATE contas SET contaSaldo = $saldo_atual WHERE contaID = $conta_id;
EOD;

			$res_update = $conn->executeQuery($q_update);

			if ($result && $res_saldo && $res_update) {
				$conn->commitTransaction();
				add_msg(SUCCESS, "Saida $controle cadastrada com sucesso");
				return;
			} else {
				$conn->rollbackTransaction();
				add_msg(ERROR, 
					"Não foi possivel registrar essa saida. Algo errado aconteceu");
				return;
			}
		}
	}

	private function calculaImposto($impostos, $valor, $data_entrada) {
		$conn = dbconn();
		$in = "";
		for ($i =0; $i < count($impostos); $i++) {
			if ($i == 0) {
				$in = $impostos[$i];
				continue;
			}
				$in .= ", ".$impostos[$i];
		}

		$q_imposto =
			<<<EOD
SELECT 
	impostoNome as nome,
	impostoValor as valor,
	contaID as conta
FROM imposto
WHERE impostoID in ($in);
EOD;
		$res_imposto = $conn->executeQuery($q_imposto);
		$array_valor = array();
		$total_desconto = 0;
		$controle = get_controle();
		$usuario_id = Gandalf::getUsuario()->id;

		while($o = mysql_fetch_object($res_imposto)) {
			$desconto = (moeda($valor) * $o->valor); 
			$total_desconto += $desconto;
			$q_custo =
				<<<EOD
SELECT custoID as id FROM custo WHERE custoNome = '$o->nome';
EOD;
			$res_custo = $conn->executeQuery($q_custo);
			$custo_id = mysql_fetch_object($res_custo)->id;
			$desconto = moeda($desconto);
			$q_insert =
				<<<EOD
INSERT INTO 
extrato(extratoDataMovimentacao, extratoControle, extratoTipo, extratoValor, extratoDataTransacao, contaID, usuarioID, custoID, extratoDescricao) 
VALUES(STR_TO_DATE('$data_entrada', '%d/%m/%Y'), '$controle', 1, $desconto, CURRENT_DATE, $o->conta, $usuario_id, $custo_id, 'IMPOSTO');
EOD;
		
			$res_insert = $conn->executeQuery($q_insert);
		}

		return $total_desconto;
	}
}
