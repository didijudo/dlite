<?php
final class CriarPerfilController extends LayoutController {
	public $js = array('perfis');
	private $perfis;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Criar Perfil </h1>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<d:perfil/>
						<div class="col-xs-6 col-xs-offset-3">
							<d:accordion head={"Perfis Cadastrados"}>
								{$this->perfis}	
							</d:accordion>
						</div>
					</div>
				</div>
			</x:frag>;
	}
	public function processRequest() {
		Gandalf::check();
		js_call("$('#idNome').maiusculo()");
		$conn = dbconn();

		$q_perfis =
			<<<EOD
SELECT 
	perfilNome as nome, 
	perfilID as id
FROM perfil 
ORDER BY perfilNome ASC;
EOD;
		$res_perfis = $conn->executeQuery($q_perfis);
		$this->perfis = <ul class="list-unstyled col-xs-12"/>;

		while($o = mysql_fetch_object($res_perfis)) {
			$link =
				<a onclick={"excluir(".$o->id.")"}
					class="btn btn-danger btn-xs prepend-left" title="Remover">
					<i class="fa fa-trash"/>
				</a>;

			$this->perfis->appendChild(
				<li>
					<h5>
						{$o->nome}
						<span>
							{$link}
						</span>
					</h5>
				</li>
			);
		}

		if (is_post()) {
			$r = $this->getRequest();
			$urls = $r->getArray('url');
			$nome_perfil = $r->getString('nome');

			if (!$nome_perfil) {
				add_msg(ERROR, 'Digite um nome para o perfil');
				return;
			}

			if (count($urls) == 0) {
				add_msg(ERROR, 'Escolha alguma permissão para o perfil');
				return;
			}

			$q_sel =
				<<<EOD
SELECT 1 AS ok FROM perfil WHERE perfilNome = '$nome_perfil';
EOD;
			$res_sel = $conn->executeQuery($q_sel);

			$q_insert =
				<<<EOD
INSERT INTO perfil(perfilNome) VALUES('$nome_perfil');
EOD;
			$conn->beginTransaction();
			$res_insert = $conn->executeQuery($q_insert);

			if ($res_insert) {
				$q_sel =
					<<<EOD
SELECT perfilID AS id FROM perfil WHERE perfilNome = '$nome_perfil';
EOD;
				$res_sel = $conn->executeQuery($q_sel);
				$id_perfil = mysql_fetch_object($res_sel)->id;

				for($i=0; $i<count($urls); $i++) {
					$q_final =
						<<<EOD
INSERT INTO perfilUrl(perfilID, urlNome) VALUES($id_perfil, '$urls[$i]')
EOD;
					$res_final = $conn->executeQuery($q_final);
					if (!$res_final) {
						add_msg(ERROR, 'Alguma coisa aconteceu. Procure o administrador.');
						$conn->rollbackTransaction();
						return;
					}
				}
				$conn->commitTransaction();
				add_msg(SUCCESS, 'Perfil cadastrado com sucesso');
				send_redirect('/listar/usuarios');
			}
		}
	}
}
