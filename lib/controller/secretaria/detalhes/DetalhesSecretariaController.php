<?php
final class DetalhesSecretariaController extends LayoutController {
	public $js = array('detalhes');
	private $secretaria;
	private $programas;
	private $numProgramas;
	private $saldo;

	public function setContent() {
		return
			<x:frag>
				<div class="page-heading">
					<h1> Detalhes da {$this->secretaria} </h1>
				</div>	
				<div class="row">
					<div class="col-md-12">
						<h3>
							<i class="fa fa-info-circle" style="color:#03A9F4"></i> 
							Informações
						</h3> 
						<i class="fa fa-asterisk" style="color:#8BC34A"></i> 
						Numero de programas: {$this->numProgramas} <br/>
						<i class="fa fa-money" style="color:#8BC34A"></i> 
						Saldo atual nas contas da secretaria : 
							<b>R$ {money_format('%!i', $this->saldo)} </b>
					</div>
					<div class="col-md-12">
						{$this->programas}
					</div>
				</div>
			</x:frag>;
	}

	public function processRequest() {
		$r = $this->getRequest();
		$id = $r->getInt('id');
		$conn = dbconn();

		$q_nome =
			<<<EOD
SELECT secretariaNome as nome FROM secretaria WHERE secretariaID = $id;
EOD;
		$res_nome = $conn->executeQuery($q_nome);
		$this->secretaria = mysql_fetch_object($res_nome)->nome;

		$q_sum =
			<<<EOD
SELECT 
	SUM(contaSaldo) AS saldo 
FROM contas 
WHERE programaID IN 
	(SELECT programaID FROM programa WHERE secretariaID = $id)
EOD;
		$res_sum = $conn->executeQuery($q_sum);
		$this->saldo = mysql_fetch_object($res_sum)->saldo;

		if (!$this->saldo) {
			$this->saldo = 0.0;
		}

		$q_count =
			<<<EOD
SELECT COUNT(programaID) AS qtd FROM programa WHERE secretariaID = $id;
EOD;

		$res_count = $conn->executeQuery($q_count);
		$this->numProgramas = mysql_fetch_object($res_count)->qtd;
		$q =
			<<<EOD
SELECT
	programaID as id,
	programaNome as nome,
	secretariaID as secretaria
FROM programa
WHERE secretariaID = $id
ORDER BY programaNome ASC
EOD;
		$res = $conn->executeQuery($q);
		$this->programas = <div class="col-xs-12 prepend-top"/>;
		while($o = mysql_fetch_object($res)) {

			$q_saldo =
				<<<EOD
SELECT 
	SUM(contaSaldo) AS saldo
FROM contas
WHERE programaID = $o->id
EOD;
			$res_saldo = $conn->executeQuery($q_saldo);
			$soma = mysql_fetch_object($res_saldo)->saldo;
			if (!$soma) {
				$soma = 0.00;
			}

			$q_contas =
				<<<EOD
SELECT 
	contaSaldo AS saldo,
	contaID as id,
	contaNome AS nome,
	contaNumero AS numero,
	contaAgencia AS agencia
FROM contas
WHERE programaID = $o->id
ORDER BY saldo DESC
EOD;
			$contas_programa = <x:frag/>;
			$res_contas = $conn->executeQuery($q_contas);
			$num_contas = 0;
			while($conta = mysql_fetch_object($res_contas)) {
				$num_contas++;
				$contas_programa->appendChild(
					<div>
						<label>
							<a href={"/alterar/conta?id=".$conta->id}
					 			title="Alterar conta" class="btn btn-sm btn-primary">
								<i class="fa fa-pencil"/>
							</a>
							<a href={"/conta/detalhes?id=".$conta->id} 
								title="Clique para mais detalhes">
								Agência: {$conta->agencia} |
								Número: {$conta->numero} 
							</a>
							<b> :::::: SALDO {'>'} R$ {money_format('%!i', $conta->saldo)}</b>
					 </label>
				 </div>
				);
				
			}
			$alterar =
				<x:frag>
					<a href={"/alterar/programa?id=".$o->id}
						title="Alterar programa" class="pull-right"
						style="margin-right: 5px;">
						<i class="fa fa-pencil"/>
					</a>
				</x:frag>;
			if ($num_contas == 0) {
				$alterar->prependChild(
					<a onclick={"excluir(".$o->id.");"}
						title="Alterar programa" class="pull-right">
						<i class="fa fa-close" style="color: #d0181e"/>
					</a>
				);
			}
			$this->programas->appendChild(
				<d:accordion head={$o->nome}
					complemento={$alterar}>
					<div class="col-md-6">
						<h5>
							<i class="fa fa-info-circle" style="color:#03A9F4"></i> 
							Informações
						</h5> 
						Saldo total nas contas do programa: <b>R$ {money_format('%!i', $soma)}</b><hr/> 
						{$contas_programa}
					</div>
					<div class="col-md-6">
						<h5>Em breve</h5><br/>
						<!--<i class="fa fa-square" style="color:#689f38"></i> 
							+ 3.500,00 | Pró jovem<br/>
							<i class="fa fa-square" style="color:#e51c23"></i> 
							+ 1.000,00 | Agricultura familiar<br/>-->
					</div>
				</d:accordion>
			);
		}
	}
}
