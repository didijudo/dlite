<?php
final class BancoMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE banco(
	bancoID int NOT NULL AUTO_INCREMENT,
	bancoNome varchar(255),
	bancoImg varchar(255),
	PRIMARY KEY (bancoID)
);
EOD;
		return $q;
	}

	public function undo() {}
}
