<?php
final class ContasMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE contas(
	contaID int NOT NULL AUTO_INCREMENT,
	contaNome varchar(255),
	contaAgencia varchar(255),
	contaNumero varchar(255),
	bancoID int,
	contaSaldo decimal(10,2),
	programaID int,
	PRIMARY KEY (contaID)
);
EOD;
		return $q;
	}

	public function undo() {}
}
