<?php
final class CorSecretariaMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
ALTER TABLE secretaria ADD COLUMN secretariaCor varchar(255);
EOD;
		return $q;
	}

	public function undo() {}
}
