<?php
final class CustoTipoMigration extends Migration {

	public function run() {
		$q =
			<<<EOD
ALTER TABLE custo ADD COLUMN custoTipo int DEFAULT 2;
EOD;
		return $q;
	}
}
