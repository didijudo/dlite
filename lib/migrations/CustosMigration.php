<?php
final class CustoMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE custo(
	custoID int NOT NULL AUTO_INCREMENT,
	custoNome varchar(255),
	PRIMARY KEY (custoID)
);
EOD;
		return $q;
	}

	public function undo() {}
}
