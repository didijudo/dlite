<?php
final class DescricaoExtratoMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
ALTER TABLE extrato ADD COLUMN extratoDescricao TEXT;

EOD;
		return $q;
	}

	public function undo() {}
}
