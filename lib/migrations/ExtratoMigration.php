<?php
final class ExtratoMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE extrato(
	extratoID int NOT NULL AUTO_INCREMENT,
	custoID varchar(255),
	extratoDataMovimentacao date,
	extratoControle varchar(255),
	extratoTipo int,
	extratoValor decimal(10,2),
	extratoDestinatario varchar(255),
	extratoDataTransacao date,
	contaID int,
	usuarioID int,	
	PRIMARY KEY (extratoID)
);
EOD;
		return $q;
	}

	public function undo() {}
}
