<?php
final class FornecedorMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE fornecedor(
	fornecedorID int NOT NULL AUTO_INCREMENT,
	fornecedorNome varchar(255),
	fornecedorCnpj varchar(255),
	PRIMARY KEY (fornecedorID)
);
EOD;
		return $q;
	}

	public function undo() {}
}
