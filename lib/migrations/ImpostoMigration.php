<?php
final class ImpostoMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE imposto(
	impostoID int NOT NULL AUTO_INCREMENT,
	impostoNome varchar(255),
	impostoValor decimal(4,4),
	contaID int,
	PRIMARY KEY (impostoID)
);
EOD;
		return $q;
	}
	public function undo() {}
}
