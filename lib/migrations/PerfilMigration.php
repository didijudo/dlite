<?php
final class PerfilMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE perfil(
	perfilID int NOT NULL AUTO_INCREMENT,
	perfilNome varchar(255) ,
	PRIMARY KEY (perfilID),
	UNIQUE(perfilNome)
);
EOD;
		return $q;
	}

	public function undo() {}
}
