<?php
final class PerfilUrlMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE perfilUrl(
	perfilID int NOT NULL,
	urlNome varchar(255) NOT NULL,
	PRIMARY KEY (perfilID, urlNome)
);
EOD;
		return $q;
	}

	public function undo() {}
}
