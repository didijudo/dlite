
<?php
final class PrimeiroAcessoMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
ALTER TABLE usuarios ADD COLUMN usuarioAcesso int DEFAULT 0;
EOD;
		return $q;
	}

	public function undo() {}
}
