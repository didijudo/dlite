<?php
final class ProgramaMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE programa(
	programaID int NOT NULL AUTO_INCREMENT,
	programaNome varchar(255),
	secretariaID int,
	PRIMARY KEY (programaID)
);
EOD;
		return $q;
	}

	public function undo() {}
}
