<?php
final class SecretariaColummMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
ALTER TABLE contas ADD COLUMN secretariaID int
EOD;
		return $q;
	}

	public function undo() {}
}
