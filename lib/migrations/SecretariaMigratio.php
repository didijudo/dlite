<?php
final class SecretariaMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE secretaria(
	secretariaID int NOT NULL AUTO_INCREMENT,
	secretariaNome varchar(255),
	PRIMARY KEY (secretariaID)
);
EOD;
		return $q;
	}

	public function undo() {}
}
