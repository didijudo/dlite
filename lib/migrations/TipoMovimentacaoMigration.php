<?php
final class TipoMovimentacaoMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE tipoMovimentacao(
	tipoID int NOT NULL AUTO_INCREMENT,
	tipoNome varchar(255),
	PRIMARY KEY (tipoID)
);
EOD;
		return $q;
	}

	public function undo() {}
}
