<?php
final class URLMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE url(
	urlID int NOT NULL AUTO_INCREMENT,
	urlNome varchar(255),
	urlLabel varchar(255),
	PRIMARY KEY (urlID),
	UNIQUE (urlNome)
);
EOD;
		return $q;
	}

	public function undo() {}
}
