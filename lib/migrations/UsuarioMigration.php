<?php
final class UsuarioMigration extends Migration {
	
	public function run() {
		$q =
			<<<EOD
CREATE TABLE usuarios(
	usuarioID int NOT NULL AUTO_INCREMENT,
	usuarioNome varchar(255),
	usuarioEmail varchar(255),
	usuarioSenha varchar(255),
	usuarioBloqueio int,
	usuarioNivel int,
	gestor int DEFAULT 0,
	PRIMARY KEY (usuarioID)
);
EOD;
		return $q;
	}

	public function undo() {}
}
