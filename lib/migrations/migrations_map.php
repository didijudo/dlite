<?php
/*
 * This file is the sequence that the migrations will be running.
 * 
 */
$migrations_map = array(
  0 => 'URLMigration',
  1 => 'PerfilMigration',
  2 => 'PerfilUrlMigration',
	3 => 'UsuarioMigration',
	4 => 'BancoMigration',
	5 => 'CustoMigration',
	6 => 'ProgramaMigration',
	7 => 'SecretariaMigration',
	8 => 'ExtratoMigration',
	9 => 'ContasMigration',
	10 => 'PrimeiroAcessoMigration',
	11 => 'SecretariaColummMigration',
	12 => 'TipoMovimentacaoMigration',
	13 => 'CorSecretariaMigration',
	14 => 'FornecedorMigration',
	15 => 'DescricaoExtratoMigration',
	16 => 'ImpostoMigration',
	17 => 'CustoTipoMigration',
);
