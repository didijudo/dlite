#!/usr/bin/php -q
<?php
include_once "__init__.php";
include_once "../application_configuration.php";

$conn = dbconn();
echo "************************************************************\n";
echo "*** URL \t\t\t\t\t\t STATUS ***\n";
echo "************************************************************\n";
while($key = current($map)) {
	
	$res = cadastraUrl($conn, key($map));
	if ($res) {
		echo key($map)."\t\t\t\t\t\t Inserindo...\n";
	} else {
		echo key($map)."\t\t\t\t\t\t SKIP\n";
	}
	next($map);
} 

echo "\n******* Update Admin profile *******\n";
$q =
	<<<EOD
INSERT INTO perfil(perfilNome) VALUES('ADMINISTRADOR')
EOD;

$res = $conn->executeQuery($q);
if ($res) {
	echo "ADMIN profile created ---------- ok\n";
} else {
	echo "ADMIN profile created ---------- no\n";
}

$q = 
	<<<EOD
INSERT INTO perfilUrl(perfilID, urlNome) 
(SELECT 1, urlNome FROM url 
	WHERE urlNome NOT IN (SELECT urlNome FROM perfilUrl WHERE perfilID = 1));
EOD;

echo "\n******* Update Acess for Admin *******\n";
$res = $conn->executeQuery($q);
if ($res) {
	echo "ADMIN access update ---------- ok\n";
} else {
	echo "ADMIN access update ---------- no\n";
}

$q = 
	<<<EOD
INSERT INTO tipoMovimentacao(tipoID, tipoNome) VALUES(1, 'ENTRADA'), (2, 'SAÍDA');
EOD;

echo "\n******* Insert TipoMovimentacao *******\n";
$res = $conn->executeQuery($q);
if ($res) {
	echo "TipoMovimentacao insert ---------- ok\n";
} else {
	echo "TipoMovimentacao insert ---------- no\n";
}


function cadastraUrl($conn, $url) {
	$q =
		<<<EOD
INSERT INTO url(urlNome) VALUES('$url');
EOD;
	$res = $conn->executeQuery($q);

	if ($res) {
		return true;
	} else {
		return false;
	}
}
